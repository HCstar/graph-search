const webpack = require('webpack');

module.exports = {
    // 部署应用包时的基本 URL。用法和 webpack 本身的 output.publicPath 一致
    publicPath: '',
    // 当运行 vue-cli-service build 时生成的生产环境构建文件的目录。
    // outputDir: '../atlas/src/main/resources/static',
    // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
    assetsDir: '',
    // 指定生成的 index.html 的输出路径 (相对于 outputDir)。也可以是一个绝对路径。
    indexPath: 'index.html',
    // 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存。
    filenameHashing: false,
    // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
    productionSourceMap: false,
    // 如果这个值是一个对象，则会通过 webpack-merge 合并到最终的配置中。
    // 如果这个值是一个函数，则会接收被解析的配置作为参数。该函数既可以修改配置并不返回任何东西，也可以返回一个被克隆或合并过的配置版本。
    configureWebpack: { // webpack相关配置
        plugins: [
            new webpack.ProvidePlugin({
                $:"jquery",
                jQuery:"jquery",
                "windows.jQuery":"jquery"
            })
        ]
    },
    // 所有 webpack-dev-server 的选项都支持。
    devServer: {

    },
    // lintOnSave: false,  // 禁用ESLint
}
