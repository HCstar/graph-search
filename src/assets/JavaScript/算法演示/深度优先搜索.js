import {setItemActive} from './AM-index.js'

export function DFS_Search(startNode, endNode) {
    // 初始化一些参数
    window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.getNodes().forEach(node => {
        node._cfg.diyCfg = {};
    });
    window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.getEdges().forEach(edge => {
        edge._cfg.diyCfg = {};
    });
    deepSearch(startNode, endNode);
}


export function deepSearch(startNode, endNode) {
    if (startNode._cfg.diyCfg.isSearch) return false;  // 已经搜索过该节点
    startNode._cfg.diyCfg.isSearch = true;
    if (startNode._cfg.id === endNode._cfg.id) {// 搜索到了结束节点，搜索结束
        // TODO 设置所有节点为灰色，然后反向点亮路径
        console.log("搜索到目标节点");
        return true;
    }
    setItemActive(startNode, 'node', undefined); // 激活该节点

    let edges = startNode.getEdges();
    if (edges.length === 0) return false;   // 没有边？？那搜个毛线啊！
    for (let edge of edges) {
        if (edge._cfg.diyCfg.isSearch === true) continue;  // 如果该边已经被搜索过了，则跳过
        edge._cfg.diyCfg.isSearch = true;
        if (edge._cfg.source._cfg.id === startNode._cfg.id) {
            setItemActive(edge, 'edge', startNode); // 激活该节点
            if (deepSearch(edge._cfg.target, endNode) === true) return true;    // 搜索成功
        } else {
            if (edge._cfg.model.style.endArrow) continue;   // 有向边特殊处理
            setItemActive(edge, 'edge', startNode); // 激活该节点
            if (deepSearch(edge._cfg.source, endNode) === true) return true;    // 搜索成功
        }
    }
}
