import * as file from "@/assets/JavaScript/global/file/fileIndex";
import {keyNode,setItemActive} from "@/assets/JavaScript/算法演示/AM-index";
import el from "element-ui/src/locale/lang/el";
export function view(open){
    let cout=[];
    for (let i=0;i<open.length;i++){
        let value=[];
        value[0]={lable:open[i]._cfg.model.label,to_star:open[i]._cfg.model.to_star,father_id:open[i]._cfg.model.father_id};
        cout[i]=value[0];
    }
    console.log(cout);
}
export function getNodeInEdge(edge,star){//返回边的另一点
    console.log(edge);
    let source_=edge._cfg.model.source;
    let target_=edge._cfg.model.target;
    let graph=window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj;
    if (star._cfg.model.id===source_){
        return graph.findById(target_);
    }else {
        return graph.findById(source_);
    }

}
export function dijste(){
    let start_node=keyNode.startNode;
    let end_node=keyNode.endNode;
    let S_arr=[];//存放已找出最短路径的顶点
    let D_arr=[];//存放未找出最短路的顶点
    let cout=[];//存放最短路径
    let graph=window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj;
    let node=graph.getNodes();//获取图中所有点
    const excel = new file.ExcelControl(['循环次数', 'D表', 'S表']);
    //第一步，先将顶点放入S数组中
    start_node._cfg.model.to_star=0;
    S_arr.push(start_node);
    for (let i=0;i<node.length;i++){//将除顶点外的点放入D
        if (node[i]!==start_node)
            D_arr.push(node[i]);
    }
    excel.showExcelAsTable(undefined);
    let counter=1;
//把信息放入表格中
    let Sx=[];
    let Dx=[];
    for(let i=0;i<D_arr.length;i++){
        Dx.push(D_arr[i]._cfg.model.label);
    }
    for(let i=0;i<S_arr.length;i++){
        Sx.push(S_arr[i]._cfg.model.label);
    }
    excel.appendChild([counter.toString(), Dx.toString(), Sx.toString()]);
    console.log("第一步完毕");
    view(S_arr);
    view(D_arr);
    while (D_arr.length!==0){
        counter++;
        let edge_away=S_arr[S_arr.length-1].getEdges();//获取该点周围路径
        console.log("测试点1",edge_away);
        for (let i=0;i<edge_away.length;i++){//去掉已经在S中的节点所在的边
            for (let j=0;j<S_arr.length;j++){
                if (getNodeInEdge(edge_away[i],S_arr[S_arr.length-1])===S_arr[j])
                    edge_away.splice(i,1);
            }
        }
        let min=edge_away[0];
        console.log("for循环开始");
        for (let i=0;i<edge_away.length;i++){
            console.log("测试点2,边数=",edge_away.length);
            if (edge_away[i]._cfg.model.weight<min._cfg.model.weight)
                min=edge_away[i];//找出该点周围的最短路径
            let target=getNodeInEdge(edge_away[i],S_arr[S_arr.length-1]);
            let lg=edge_away[i]._cfg.model.weight+S_arr[S_arr.length-1]._cfg.model.to_star;//查看边上的点到顶点的距离要不要更新
            view(target);
            console.log("查看边值",lg);
            if (lg<target._cfg.model.to_star)
                target._cfg.model.to_star=lg;
        }
        view(D_arr);
        console.log("测试点3");
        getNodeInEdge(min,S_arr[S_arr.length-1])._cfg.model.father_id=S_arr[S_arr.length-1]._cfg.model.id;//将最短边的点的父节点设为边上的顶点
        D_arr.splice(D_arr.indexOf(getNodeInEdge(min,S_arr[S_arr.length-1])),1);//在D中删除最短边的另一点
        S_arr.push(getNodeInEdge(min,S_arr[S_arr.length-1]));//将最短边上另一点加入S
        console.log("测试点4");
        view(S_arr);
        view(D_arr);
        //把信息放入表格中
        let Sx=[];
        let Dx=[];
        for(let i=0;i<D_arr.length;i++){
            Dx.push(D_arr[i]._cfg.model.label);
        }
        for(let i=0;i<S_arr.length;i++){
            Sx.push(S_arr[i]._cfg.model.label);
        }
        excel.appendChild([counter.toString(), Dx.toString(), Sx.toString()]);
        console.log("第一步完毕");
        if (S_arr[S_arr.length-1]===end_node){
            console.log("找到终点");
            let cout=[];
            cout.push(S_arr[S_arr.length-1]);
            let father=cout[0]._cfg.model.father_id;
            while (cout[0]._cfg.model.id!==start_node._cfg.model.id){
                console.log("测试点5");
                for (let i=0;i<S_arr.length;i++){
                    if (S_arr[i]._cfg.model.id===father){
                        father=S_arr[i]._cfg.model.father_id;
                        cout.unshift(S_arr[i]);
                    }
                }
            }
            cout.forEach((node, index, array) => {
                if (index === 0) return;
                array[index - 1].getEdges().forEach(edge => {
                    console.log('测试点：', array[index - 1], node);
                    if (edge._cfg.source._cfg.id === array[index - 1]._cfg.id) {
                        if (edge._cfg.target._cfg.id === node._cfg.id) {
                            setItemActive(edge, 'edge', array[index - 1]);
                            setItemActive(node, 'node');
                        }
                    } else {
                        if (edge._cfg.source._cfg.id === node._cfg.id) {
                            setItemActive(edge, 'edge', array[index - 1]);
                            setItemActive(node, 'node');
                        }
                    }
                })
            })
            view(cout);
            return ;
        }
    }

}