import {Queue} from "@/assets/JavaScript/global/数据结构/Queue";
import {setItemActive} from './AM-index.js';

export function *NFS_Search(startNode, endNode) {
    // 初始化一些参数
    window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.getNodes().forEach(node => {
        node._cfg.diyCfg = {};
    });
    window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.getEdges().forEach(edge => {
        edge._cfg.diyCfg = {};
    });
    let queue = new Queue(100); // 先初始化一个长度为100的队列
    queue.push(startNode);

    while (!queue.isEmpty()) {  // 只要队列不为空，就一直执行下去
        let node = queue.pop();
        if (node._cfg.diyCfg.isSearch)  continue;
        node._cfg.diyCfg.isSearch = true;
        if (node._cfg.id === endNode._cfg.id)   break;  // 搜索成功

        let edges = node.getEdges();    // 获取该节点的所有边
        if (!edges.length) continue;
        for (let edge of edges) {
            if (edge._cfg.diyCfg.isSearch)  continue;
            edge._cfg.diyCfg.isSearch = true;
            if (edge._cfg.source._cfg.id === node._cfg.id) {          // 边的起点是node
                setItemActive(edge, 'edge', node);              // 激活边
                setItemActive(edge._cfg.target, 'node', undefined);// 激活节点
                if (edge._cfg.target._cfg.id === endNode._cfg.id)   return 'success';  // 搜索成功
                queue.push(edge._cfg.target);
            } else {    // 边的起点不是node
                if (edge._cfg.model.style.endArrow) continue;   // 有向边特殊处理
                setItemActive(edge, 'edge', startNode); // 激活边
                setItemActive(edge._cfg.source, 'node', undefined);// 激活节点
                if (edge._cfg.source._cfg.id === endNode._cfg.id)   return 'success';  // 搜索成功
                queue.push(edge._cfg.source);
            }
        }
        yield node; // 用于分步执行
    }
    return 'failure';   // 搜索失败
}
