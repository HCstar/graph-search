import * as file from "@/assets/JavaScript/global/file/fileIndex";
import {keyNode,setItemActive} from "@/assets/JavaScript/算法演示/AM-index";

export function dist(start,end){
    let dx=Math.abs(start._cfg.model.x-end._cfg.model.x);
    let dy=Math.abs(start._cfg.model.y-end._cfg.model.y);
    return parseInt(Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2)));
}
export function view(open){
    let cout=[];
    for (let i=0;i<open.length;i++){
        let value=[];
        value[0]={lable:open[i]._cfg.model.label,f_value:open[i]._cfg.model.f_value,h_value:open[i]._cfg.model.h_value,father_id:open[i]._cfg.model.father_id};
        cout[i]=value[0];
    }
    console.log(cout);
}
export function A_go(){
    const excel = new file.ExcelControl(['循环次数', 'open表', 'close表']);

/*    let div = document.createElement('div');
    document.body.appendChild(div);
    div.style.position = 'absolute';
    div.style.zIndex = 999;
    div.style.backgroundColor = 'rgba(255, 255, 255, 0)';
    div.style.top = 0;
    div.style.height = 0;
    div.style.width = '100%';
    div.style.height = '100%';
    div.id = 'div_A';
    excel.showExcelAsTable('div_A');*/
    excel.showExcelAsTable(undefined);

    let graph=window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj;
    let nodes=graph.getNodes();
    let openlist=[];//open表
    let closelist=[];//close表
    let start_node=keyNode.startNode;
    let end_node=keyNode.endNode;
    // for(let i=0;i<nodes.length;i++){//找到起点和终点
    //     if(nodes[i]._cfg.model.label==="起点(0)")
    //         start_node=nodes[i];
    //     if(nodes[i]._cfg.model.label==="终点(0)")
    //         end_node=nodes[i];
    // }
    //第一步，算出起点的f值，将起点放入open表
    start_node._cfg.model.g_value=0;
    start_node._cfg.model.h_value=0;
    start_node._cfg.model.f_value=dist(start_node,end_node);//起点的f值和h值相等
    console.log(start_node);
    openlist.push(start_node);
    console.log("第一步完毕");
    view(openlist);
    let counter=1;
    while (openlist.length!==0 ){//如果open表空或者终点已经在close表中，则结束算法
        //第二步，对open表进行排序
        console.log("第二步");
        for(let i=0;i<openlist.length-1;i++){//冒泡排序，n-1次
            for(let j=0;j<openlist.length-i-1;j++){//每次冒泡排序n-i-1次
                let temp=openlist[j];
                if(openlist[j]._cfg.model.f_value>openlist[j+1]._cfg.model.f_value){//优先比较f
                    openlist[j]=openlist[j+1];
                    openlist[j+1]=temp;
                }else if (openlist[j]._cfg.model.f_value===openlist[j+1]._cfg.model.f_value){//f相同就比较h
                    if (openlist[j]._cfg.model.h_value>openlist[j+1]._cfg.model.h_value){
                        openlist[j]=openlist[j+1];
                        openlist[j+1]=temp;
                    }
                }
            }
        }
        console.log("排序完毕");
       view(openlist);
        let openx=[];
        let closex=[];
        for(let i=0;i<openlist.length;i++){
            openx.push(openlist[i]._cfg.model.label);
        }
        for(let i=0;i<closelist.length;i++){
            closex.push(closelist[i]._cfg.model.label);
        }
        excel.appendChild([counter.toString(), openx.toString(), closex.toString()]);
        //open表排序完成
        //在open表中中出最小节点，将该点从open表删除，加入close表
        closelist.push(openlist[0]);//将最小节点加入close表
        let center=openlist[0];
        openlist.splice(0,1);//从open表中删除该节点
        console.log("close表更新完毕");
        view(closelist);
        console.log("open表删除节点完毕");
        view(openlist);
        console.log(center._cfg.model.label);
        //将该点设为中心点，遍历周围节临近点，如果临近点不在close表和open表中，则加入open表，如果临近点在open表中，则判断是否要更新
        let edge_away=center.getEdges();//获取连接的边
        let node_away=[];//用来存放临近点
        for(let i=0;i<edge_away.length;i++){//将边上不和中心点相同id的点加入node_away里面
            console.log("外循环"+i+"次");
            console.log("边数"+edge_away.length);
            let flag=1;
            if(edge_away[i]._cfg.model.source!==center._cfg.model.id){
                let find_node=graph.findById(edge_away[i]._cfg.model.source);
                console.log(find_node._cfg.model.label);
                for (let j=0;j<closelist.length;j++){
                    if (closelist[j]===find_node){
                        console.log("1和close有交集"+"i="+i);
                        flag=0;
                        break ;
                    }else if (openlist[j]===find_node){
                        let gval=center._cfg.model.g_value+edge_away[i]._cfg.model.weight;
                        if (gval<openlist[j]._cfg.model.g_value){
                            openlist[j]._cfg.model.g_value=gval;
                            openlist[j]._cfg.model.f_value=gval+openlist[j]._cfg.model.h_value;
                            console.log("1open表更新"+"i="+i);
                            flag=0;
                            break ;
                        }
                    }
                }
                if (flag===1){
                    find_node._cfg.model.h_value=dist(find_node,end_node);//赋值该节点h值为其到终点的直线距离
                    find_node._cfg.model.g_value=center._cfg.model.g_value+edge_away[i]._cfg.model.weight;//该节点g值为中心点g值加上它们两点的线长
                    find_node._cfg.model.f_value=find_node._cfg.model.g_value+find_node._cfg.model.h_value;//赋值f值
                    find_node._cfg.model.father_id=center._cfg.model.id;
                    node_away.push(find_node);
                }


            }else if (edge_away[i]._cfg.model.target!==center._cfg.model.id){
                let find_node=graph.findById(edge_away[i]._cfg.model.target);
                console.log(find_node._cfg.model.label);
                for (let j=0;j<closelist.length;j++){
                    if (closelist[j]===find_node){
                        console.log("2和close有交集"+"i="+i);
                        flag=0;
                        break ;
                    }else if (openlist[j]===find_node){
                        let gval=center._cfg.model.g_value+edge_away[i]._cfg.model.weight;
                        if (gval<openlist[j]._cfg.model.g_value){
                            openlist[j]._cfg.model.g_value=gval;
                            openlist[j]._cfg.model.f_value=gval+openlist[j]._cfg.model.h_value;
                            console.log("2open表更新"+"i="+i);
                            flag=0;
                            break ;
                        }
                    }
                }
                if (flag===1){
                    find_node._cfg.model.h_value=dist(find_node,end_node);//赋值该节点h值为其到终点的直线距离
                    find_node._cfg.model.g_value=center._cfg.model.g_value+edge_away[i]._cfg.model.weight;//该节点g值为中心点g值加上它们两点的线长
                    find_node._cfg.model.f_value=find_node._cfg.model.g_value+find_node._cfg.model.h_value;//赋值f值
                    find_node._cfg.model.father_id=center._cfg.model.id;
                    node_away.push(find_node);
                }

            }
        }
        console.log("节点加入node_away完毕");
        view(node_away);
/*        for(let i=0;i<closelist.length;i++){//在node_away中删除已经在closelist中出现的节点
            for (let j=0;j<node_away.length;j++){
                if (node_away[j]._cfg.model.id===closelist[i]._cfg.model.id)
                    node_away.splice(j,1);
            }
        }
        console.log("node_away删除与close表相同节点完毕");
        view(node_away);
        for (let i=0;i<node_away.length;i++){//判断节点是否在open表中，在则判断是否需要更新f值
            for (let j=0;j<openlist.length;j++){
                if (node_away[i]._cfg.model.id===openlist[j]._cfg.model.id){//如果在open表中
                    if(node_away[i]._cfg.model.f_value===openlist[j]._cfg.model.f_value){//如果f值相同则比较h值
                        if (node_away[i]._cfg.model.h_value<openlist[j]._cfg.model.h_value){//如果新点h值更小则更新open表
                            openlist[j]=node_away[i];
                            break;
                        }
                    }else if (node_away[i]._cfg.model.f_value<openlist[j]._cfg.model.f_value){//如果新点f值更小，则更新open表
                        openlist[j]=node_away[i];
                        break;
                    }
            }
        }
        }*/
            for (let i=0;i<node_away.length;i++){//向openlist中添加node_away中的数据
                if (openlist.indexOf(node_away[i])===-1){
                    openlist.push(node_away[i]);
                }
            }
        console.log("向openlist中添加数据完毕");
        view(node_away);
        view(openlist);
        if (openlist.indexOf(end_node)!==-1){
            counter++;
            let openx=[];
            let closex=[];
            for(let i=0;i<openlist.length;i++){
                openx.push(openlist[i]._cfg.model.label);
            }
            for(let i=0;i<closelist.length;i++){
                closex.push(closelist[i]._cfg.model.label);
            }
            excel.appendChild([counter.toString(), openx.toString(), closex.toString()]);

            closelist.push(openlist[openlist.indexOf(end_node)]);
            console.log("终点置入");
            view(closelist);
            openlist.splice(openlist.indexOf(end_node),1);
            let cout=[];
            cout.push(closelist[closelist.length-1]);
            let father=cout[0]._cfg.model.father_id;
            while (cout[0]._cfg.model.id!==start_node._cfg.model.id){
                for (let i=0;i<closelist.length;i++){
                    if (closelist[i]._cfg.model.id===father){
                        father=closelist[i]._cfg.model.father_id;
                        cout.unshift(closelist[i]);
                    }
                }
            }
            view(cout);
            counter++;
            let openx_=[];
            let closex_=[];
            for(let i=0;i<openlist.length;i++){
                openx_.push(openlist[i]._cfg.model.label);
            }
            for(let i=0;i<closelist.length;i++){
                closex_.push(closelist[i]._cfg.model.label);
            }
            excel.appendChild([counter.toString(), openx_.toString(), closex_.toString()]);
            // excel.downloadAsExcel('A*算法表');
            let setedge=start_node.getEdges();

            cout.forEach((node, index, array) => {
                if (index === 0) return;
                array[index - 1].getEdges().forEach(edge => {
                    console.log('测试点：', array[index - 1], node);
                    if (edge._cfg.source._cfg.id === array[index - 1]._cfg.id) {
                        if (edge._cfg.target._cfg.id === node._cfg.id) {
                            setItemActive(edge, 'edge', array[index - 1]);
                            setItemActive(node, 'node');
                        }
                    } else {
                        if (edge._cfg.source._cfg.id === node._cfg.id) {
                            setItemActive(edge, 'edge', array[index - 1]);
                            setItemActive(node, 'node');
                        }
                    }
                })
            })
            view(cout);
            return ;
        }
        console.log("open表更新完毕");
        counter++;
    }
    if (openlist===false){//如果open表空，则失败
        console.log("fail to reserch!");
    }

}
