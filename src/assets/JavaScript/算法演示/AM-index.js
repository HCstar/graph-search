import * as DFS from './深度优先搜索.js';
import * as NFS from './广度优先搜索.js';
import * as AX  from './A星算法.js';
import * as DiJ  from './Dijkstra算法';

import {myGraph} from "@/assets/JavaScript/graph/graph";

export const keyNode = {
    startNode: undefined,
    endNode: undefined,
}

export function algorithm(type) {
    // 重置除开始和结束节点以外的所有节点状态
    const graph = window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj;
    graph.findAllByState('node', 'selected').forEach(node => {
        if (node._cfg.id === keyNode.startNode._cfg.id) return;
        if (node._cfg.id === keyNode.endNode._cfg.id) return;
        node.clearStates(node.getStates());
        node.setState('dormant', true);
    });
    graph.findAllByState('edge', 'growth').forEach(edge => {
        edge.clearStates(edge.getStates());
        edge.setState('dormant', true);
    });
    graph.findAllByState('edge', 'growth2').forEach(edge => {
        edge.clearStates(edge.getStates());
        edge.setState('dormant', true);
    });
    switch (type) {
        case '深度优先搜索':
            DFS.DFS_Search(keyNode.startNode, keyNode.endNode);
            break;
        case '广度优先搜索':
            let nfs = NFS.NFS_Search(keyNode.startNode, keyNode.endNode);
            let nextFunc = (event) => {
                if (event.keyCode === 39) { // 按下right键，单步执行
                    let value = nfs.next().value;
                    if (value === 'success' || value === 'failure')
                        window.removeEventListener('keyup', nextFunc, true);
                } else if (event.keyCode === 40) { // 按下enter键，一次执行结束
                    let value = undefined;
                    while (value !== 'success' && value !== 'failure') value = nfs.next().value;
                    window.removeEventListener('keyup', nextFunc, true);
                }
            }
            window.addEventListener('keyup', nextFunc, true);
            break;
        case 'A*算法':
            console.log('A*算法触发');
            AX.A_go();
            break;
        case 'Dijkstra算法':
            console.log('Dijkstra算法触发');
            DiJ.dijste();
            break;
    }
}

let activeItem = new Set();
let setActiveNext = undefined;

/**
 * 利用生成器实现依次点亮节点与边
 */
function *setActive() {
    if (activeItem.length === 0)    return 'over';
    for (let myItem of activeItem) {    // 依次激活所有节点
        if (myItem.type === 'node') {
            myItem.item.setState('selected', true);
            activeItem.delete(myItem);
        } else {
            myGraph.Edge.setEdgeActive(myItem.item, myItem.startNode, () => {
                activeItem.delete(myItem);
                setActiveNext && setActiveNext.next();
            });
            yield '';
        }
    }
    return 'over';
}

/**
 * 激活目标item（顺序将根据添加的顺序来）
 * @param item  {object}    节点对象
 * @param type  {string}    节点类型，node / edge
 * @param startNode {object | undefined}    开始节点
 */
export function setItemActive(item, type, startNode) {
    activeItem.add({item, type, startNode});
    console.log(activeItem.size);
    if (activeItem.size === 1) {
        setActiveNext = setActive();
        setActiveNext.next();
    }
}

