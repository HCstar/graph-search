/**
 * 文件读取相关代码
 * @author HC
 * @lastEditTime 2020.11.07
 * @lastEditAuthor  HC
 */
let fileRead = {
    element: undefined,     // 用于保存dom元素的引用
    callback: undefined,    // 文件打开后的回调函数
}

/**
 * input输入框 id="input-fileRead" change事件回调函数
 * @param event {object}    JS标准event对象
 */
function fileRead_inputOnchange (event) {
    if (fileRead.element === undefined) {
        console.error('fileRead未初始化，fileRead_inputOnchange');
        return false;
    }
    fileRead.callback && fileRead.callback(event.target);
}

/**
 * 文件读取模块初始化函数，第一次使用文件读取模块时应调用此函数
 */
function fileRead_Initialize() {
    if (fileRead.element !== undefined) return; // 如果已经初始化了，则忽略
    let inputDom = document.createElement('input');
    inputDom.id = 'input-fileRead';
    inputDom.multiple = true;
    inputDom.type = 'file';
    inputDom.accept = '*/*';
    inputDom.style.display = 'none';
    document.body.appendChild(inputDom);
    fileRead.element = document.getElementById('input-fileRead');
    fileRead.element.addEventListener('change', fileRead_inputOnchange, true); // 添加时间监听函数
}
fileRead_Initialize();

/**
 * 打开本地文件
 * @param type      {string}    要打开的文件类型
 * @param callback  {function}  打开后的回调函数，该函数可接受一个参数，为 event.target 对象
 * @return          {boolean}   操作是否成功
 */
export function fileRead_OpenFile(type, callback) {
    if (typeof callback !== 'function') {
        console.warn('callback不是函数，打开文件操作失败');
        return false;
    }
    fileRead.callback = callback;
    fileRead.element.value = '';
    fileRead.element.setAttribute('accept', type);
    fileRead.element.click(); // 换起文件对话框
}
