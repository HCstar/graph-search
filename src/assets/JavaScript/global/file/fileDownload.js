/**
 * 文件下载相关代码
 * @author HC
 * @lastEditTime 2020.11.07
 * @lastEditAuthor  HC
 */

let fileDownload = {
    element: undefined,
}

/**
 * 文件下载模块初始化函数，第一次使用文件下载模块时应调用此函数
 */
function fileDownload_Initialize() {
    if (fileDownload.element !== undefined) return; // 如果已经初始化了，则忽略
    let a = document.createElement('a');
    a.id = 'a-fileDownload';
    a.style.display = 'none';
    document.body.appendChild(a);
    fileDownload.element = document.getElementById('a-fileDownload');
}
fileDownload_Initialize();

/**
 * 将文件保存到本地
 * @param data  {any}    要保存的数据，需要能被blob支持的格式
 * @param name  {String} 文件名称
 * @param mime  {String} 文件MIME类型
 */
export function fileDownload_Download(data, name, mime) {
    let blob = new Blob([data], {type: mime});
    fileDownload.element.href = window.URL.createObjectURL(blob);
    fileDownload.element.download = name;
    fileDownload.element.click();  // 发起下载;
}
