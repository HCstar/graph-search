import * as file from './fileDownload.js';

/**
 * 将数据保存为excel表格（打开功能暂时搁置）
 * @author HC
 * @lastEditTime 2020.11.07
 * @lastEditAuthor HC
 */
export class ExcelControl {
    element = undefined;    // 挂载dom元素
    rowNumber = 0;          // 当前表格行数
    colNumber = 0;          // 当前表格的列数
    showTableCfg = {        // 用于在页面上显示table的dom元素相关
        element: undefined,         // 存放表格的DOM容器
        buttonShow: undefined,      // 显示按钮
        buttonHidden: undefined,    // 隐藏按钮
    }
    /**
     * ExcelControl类的构造函数
     * @param headRow   {Array} 头部行（第一行）数据，将决定表格的列数
     */
    constructor(headRow) {
        this.colNumber = headRow.length;
        if (this.colNumber === 0) {
            console.error('headRow数组中没有元素，无法初始化excel表格');
            return false;
        }
        this.rowNumber = 1;
        this.element = document.createElement('table');
        this.element.width = '100%';
        this.element.style.margin = '0 auto';
        this.element.style.borderSpacing = '0';
        this.element.style.borderCollapse = 'collapse';

        this.appendChild(headRow);
        console.log(this.element);
    }

    /**
     * 为表格中添加新行
     * @param rowData {Array}   新行的数据
     */
    appendChild(rowData) {
        let row = document.createElement('tr');
        rowData.forEach(value => {
            let col = document.createElement('td');
            col.innerHTML = value;
            col.style.textAlign = 'center';
            col.style.borderStyle = 'solid';
            col.style.borderWidth = '2px';
            col.style.borderColor = 'black';
            row.appendChild(col);
        });
        this.element.appendChild(row);
    }

    /**
     * 将表格显示在指定容器中
     * @param divId {string}    盛装表格的容器的id
     */
    showExcelAsTable(divId) {
        if (this.showTableCfg.element) return;  // 如果已经初始化，则忽略操作
        if (divId) {    // 如果给了容器id
            this.showTableCfg.element = document.getElementById(divId);
        } else {
            // 先在页面左下角添加一个按钮
            this.showTableCfg.buttonShow = document.getElementById('table-button-show');
            if (!this.showTableCfg.buttonShow) {    // 避免重复
                this.showTableCfg.buttonShow = document.createElement('button');
                this.showTableCfg.buttonShow.id = 'table-button-show';
                this.showTableCfg.buttonShow.style.cssText = `
                position: absolute;left: 0px;bottom: 0px;z-index: 20;
                border-radius: 30%;width: 30px;height: 30px;background-color:red;`;
                this.showTableCfg.buttonShow.innerHTML = `<span style="font-size: 20px;font-weight: 700;">+</span>`;
                document.body.appendChild(this.showTableCfg.buttonShow);
            }

            // 添加用于显示的DOM容器
            this.showTableCfg.element = document.getElementById('table-div-element');
            this.showTableCfg.buttonHidden = document.getElementById('table-button-hidden');
            if (!this.showTableCfg.element) {
                this.showTableCfg.element = document.createElement('div');
                this.showTableCfg.element.id = 'table-div-element';
                this.showTableCfg.element.style.cssText = `
                position: absolute;left: 0px;bottom: 0px;width: 100%;padding: 20px;display:none;
                box-sizing: border-box;z-index: 20;overflow: scroll;background-color:white;`
                document.body.appendChild(this.showTableCfg.element);

                // 在该容器中添加一个按钮，用于隐藏该DOM容器
                this.showTableCfg.buttonHidden = document.createElement('button');
                this.showTableCfg.buttonHidden.id = 'table-button-hidden';
                this.showTableCfg.buttonHidden.style.cssText = `
                float: right;border-radius: 30%;width: 30px;height: 30px;background-color:red;`;
                this.showTableCfg.buttonHidden.innerHTML = `<span style="font-size: 20px;font-weight: 700;">X</span>`;
                this.showTableCfg.element.appendChild(this.showTableCfg.buttonHidden);
            } else
                console.log(this.showTableCfg.element.removeChild(this.showTableCfg.element.childNodes[1]));

            // 为按钮添加事件处理函数
            this.showTableCfg.buttonShow.onclick = () => {
                this.showTableCfg.buttonShow.style.display = 'none';    // 隐藏自身
                this.showTableCfg.element.style.display = 'block';      // 显示表格
            }
            this.showTableCfg.buttonHidden.onclick = () => {
                this.showTableCfg.element.style.display = 'none';       // 隐藏表格
                this.showTableCfg.buttonShow.style.display = 'block';   // 显示按钮
            }
        }
        this.showTableCfg.element.appendChild(this.element);    // 将表格放入容器中
    }

    /**
     * 移除用于显示表格的DOM元素
     */
    removeTableDom() {
        document.body.removeChild(this.showTableCfg.buttonShow);
        document.body.removeChild(this.showTableCfg.element);
    }

    downloadAsExcel(fileName) {
        let excelHtml = `
<html>
    <head><meta charset='utf-8' /></head>
    <body>
        ${this.element.outerHTML}
     </body>
</html>
`;
        file.fileDownload_Download(excelHtml, `${fileName}.xls`, 'application/vnd.ms-excel');
    }
}
