/**
 * 文件控制模块入口文件，非模块化全局引入（兼容旧代码）
 */
import * as read from './fileRead.js';
import * as download from './fileDownload.js';
import * as excel from './excelControl.js';
import * as jsonControl from './jsonControl.js';
window.fileControl = {};
Object.assign(window.fileControl, read, download, excel, jsonControl);
