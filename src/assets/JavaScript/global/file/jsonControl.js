import * as fileRead from './fileRead.js';
import * as fileDownLoad from './fileDownload.js'

/**
 * 读取JSON文件，处理后作为JS对象返回
 * @param callback  {function}  文件读取后的回调函数，会返回转化后的JS对象
 */
export function readAsJson(callback) {
    fileRead.fileRead_OpenFile('.json', (target) => {
        let fileReader = new FileReader();
        fileReader.onload = () => {
            callback(JSON.parse(fileReader.result.toString()));
        }
        fileReader.readAsText(target.files[0], 'UTF-8');
    });
}

/**
 * 将目标数据保存为JSON文件
 * @param data
 * @param name
 */
export function downloadAsJson(data, name) {
    fileDownLoad.fileDownload_Download(data, name, 'application/json');
}
