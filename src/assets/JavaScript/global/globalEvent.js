import * as tools from './tools'

let GlobalEvent = {
    onResize: {     // 为了提高性能，回调函数将在页面尺寸停止变动后的0.3秒开始执行
        width: undefined,       // 游览器可视内容区宽度
        height: undefined,      // 游览器可视内容区高度
        callback: [],           // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onClick: {      // 仅鼠标左键有效
        callback: [],           // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onDblClick: {   // 仅鼠标左键有效
        callback: [],           // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onMouseDown: {
        callback: new Map(),    // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onMouseMove: {  // 不区分鼠标按键，为了提高性能，当onMouseDown触发后，事件才会被挂载，onMouseUp发生后取消
        callback: [],           // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onMouseUp: {
        callback: new Map(),    // 回调函数，当页面刷新时 ，会依次调用这些函数
    },
    onKeyDown: {
        callback: new Map(),    // 对于键盘事件，该数组中还保留每个回调对应的按键key
    },
    onKeyUp: {
        callback: new Map(),    // 对于键盘事件，该数组中还保留每个回调对应的按键key
    },
};

/* ***************************************** vue methods对象 ************************************************* */
if (typeof window.vueMethods !== 'object')  window.vueMethods = {};
window.vueMethods = Object.assign(window.vueMethods, {

})

/**
 * 挂载在window上的 onresize 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnResize(event) {
    // 如果延迟刷新存在，则清空后重新定义，避免频繁刷新
    tools.tools_TimeOut('onresize', 300, () => {
        // 获取最新的窗口尺寸
        if(window.innerWidth) { // 兼容dom
            GlobalEvent.onResize.width = window.innerWidth;
            GlobalEvent.onResize.height = window.innerHeight;
        } else if((document.body) && (document.body.clientWidth)) { // 兼容ie
            GlobalEvent.onResize.width = document.body.clientWidth;
            GlobalEvent.onResize.height = document.body.clientHeight;
        } else {
            console.error("页面尺寸刷新失败，找不到对应的dom属性");
            return;
        }
        // 依次调用队列中的回调函数
        if (GlobalEvent.onResize.callback && GlobalEvent.onResize.callback.length)
            GlobalEvent.onResize.callback.forEach(func => {func(event, GlobalEvent.onResize.width, GlobalEvent.onResize.height)});
    })
}

/**
 * 挂载在window上的 onclick 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnClick(event) {
    for (let func of GlobalEvent.onClick.callback)
        func(event);
}

/**
 * 挂载在window上的 ondblclick 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnDblClick(event) {
    for (let func of GlobalEvent.onDblClick.callback)
        func(event);
}

/**
 * 挂载在window上的 onMouseDown 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnMouseDown(event) {
    if (GlobalEvent.onMouseDown.callback.has(event.button)) {
        GlobalEvent.onMouseDown.callback.get(event.button).forEach(func => {
            func(event);
        });
    }
    // 挂载onMouseMove事件处理函数
    window.addEventListener('mousemove', GlobalEvent_OnMouseMove,false);
}

/**
 * 挂载在window上的 onMouseMove 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnMouseMove(event) {
    for (let func of GlobalEvent.onMouseMove.callback)
        func(event);
}

/**
 * 挂载在window上的 onMouseUp 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnMouseUp(event) {
    // 移除onMouseMove事件处理函数
    window.removeEventListener('mousemove', GlobalEvent_OnMouseMove,false);
    if (GlobalEvent.onMouseUp.callback.has(event.button.toString())) {
        GlobalEvent.onMouseUp.callback.get(event.button.toString()).forEach(func => {
            func(event);
        });
    }
}

/**
 * 挂载在window上的 onkeydown 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnKeyDown(event) {
    if (GlobalEvent.onKeyDown.callback.has(event.keyCode.toString())) {
        GlobalEvent.onKeyDown.callback.get(event.keyCode.toString()).forEach(func => {
            func(event);
        });
    }
}

/**
 * 挂载在window上的 onkeyup 监听函数
 * @param event 标准JS事件对象
 */
function GlobalEvent_OnKeyUp(event) {
    if (GlobalEvent.onKeyUp.callback.has(event.keyCode.toString())) {
        GlobalEvent.onKeyUp.callback.get(event.keyCode.toString()).forEach(func => {
            func(event);
        });
    }
}

/**
 * 打开全局事件监听函数
 * @param type  {string}    要打开的监听事件类型，如果为空，则打开所有监听事件
 */
export function openGlobalEvent(type) {
    // 取消游览器默认右键菜单
    window.oncontextmenu = (ev => {ev.preventDefault()});
    /*
     * true     捕获阶段执行
     * false    冒泡阶段执行
     */
    switch (type) {
        case 'resize':
            window.addEventListener('resize',GlobalEvent_OnResize,true);
            break;
        case 'click':
            window.addEventListener('click',GlobalEvent_OnClick,true);
            break;
        case 'dblclick':
            window.addEventListener('dblclick',GlobalEvent_OnDblClick,true);
            break;
        case 'mousedown':
            window.addEventListener('mousedown',GlobalEvent_OnMouseDown,true);
            break;
        case 'mouseup':
            window.addEventListener('mouseup',GlobalEvent_OnMouseUp,true);
            break;
        case 'keydown':
            window.addEventListener('keydown',GlobalEvent_OnKeyDown,true);
            break;
        case 'keyup':
            window.addEventListener('keyup',GlobalEvent_OnKeyUp,true);
            break;
        default:
            window.addEventListener('resize',GlobalEvent_OnResize,true);
            window.addEventListener('click',GlobalEvent_OnClick,true);
            window.addEventListener('dblclick',GlobalEvent_OnDblClick,true);
            window.addEventListener('mousedown',GlobalEvent_OnMouseDown,true);
            window.addEventListener('mouseup',GlobalEvent_OnMouseUp,true);
            window.addEventListener('keydown',GlobalEvent_OnKeyDown,true);
            window.addEventListener('keyup',GlobalEvent_OnKeyUp,true);
    }
}


/**
 * 关闭全局事件监听函数
 * @param type  {string}    要关闭的监听事件类型，如果为空，则关闭所有监听事件
 */
export function closeGlobalEvent(type) {
    /*
     * true     捕获阶段执行
     * false    冒泡阶段执行
     */
    switch (type) {
        case 'resize':
            window.removeEventListener('resize',GlobalEvent_OnResize,true);
            break;
        case 'click':
            window.removeEventListener('click',GlobalEvent_OnClick,true);
            break;
        case 'dblclick':
            window.removeEventListener('dblclick',GlobalEvent_OnDblClick,true);
            break;
        case 'mousedown':
            window.removeEventListener('mousedown',GlobalEvent_OnMouseDown,true);
            break;
        case 'mouseup':
            window.removeEventListener('mouseup',GlobalEvent_OnMouseUp,true);
            break;
        case 'keydown':
            window.removeEventListener('keydown',GlobalEvent_OnKeyDown,true);
            break;
        case 'keyup':
            window.removeEventListener('keyup',GlobalEvent_OnKeyUp,true);
            break;
        default:
            window.removeEventListener('resize',GlobalEvent_OnResize,true);
            window.removeEventListener('click',GlobalEvent_OnClick,true);
            window.removeEventListener('dblclick',GlobalEvent_OnDblClick,true);
            window.removeEventListener('mousedown',GlobalEvent_OnMouseDown,true);
            window.removeEventListener('mouseup',GlobalEvent_OnMouseUp,true);
            window.removeEventListener('keydown',GlobalEvent_OnKeyDown,true);
            window.removeEventListener('keyup',GlobalEvent_OnKeyUp,true);
    }
}

/**
 * 添加全局监听函数（挂载在window上）
 * @param eventName {string}    事件名称
 * @param callback  {function}  回调函数
 * @param key       {number}    键盘按键key值，当添加键盘事件时，需要指定此参数
 */
export function addGlobalListener(eventName, callback, key = undefined) {
    switch (eventName) {
        case 'resize':
            GlobalEvent.onResize.callback.push(callback);
            break;
        case 'click':
            GlobalEvent.onClick.callback.push(callback);
            break;
        case 'dblclick':
            GlobalEvent.onDblClick.callback.push(callback);
            break;
        case 'mousedown':
            if (!GlobalEvent.onMouseDown.callback.has(key.toString()))
                GlobalEvent.onMouseDown.callback.set(key.toString(), [callback]);
            else
                GlobalEvent.onMouseDown.callback.get(key.toString()).push(callback);
            break;
        case 'mousemove':
            GlobalEvent.onMouseMove.callback.push(callback);
            break;
        case 'mouseup':
            if (!GlobalEvent.onMouseUp.callback.has(key.toString()))
                GlobalEvent.onMouseUp.callback.set(key.toString(), [callback]);
            else
                GlobalEvent.onMouseUp.callback.get(key.toString()).push(callback);
            break;
        case 'keydown':
            if (!GlobalEvent.onKeyDown.callback.has(key.toString()))
                GlobalEvent.onKeyDown.callback.set(key.toString(), [callback]);
            else
                GlobalEvent.onKeyDown.callback.get(key.toString()).push(callback);
            break;
        case 'keyup':
            if (!GlobalEvent.onKeyUp.callback.has(key.toString()))
                GlobalEvent.onKeyUp.callback.set(key.toString(), [callback]);
            else
                GlobalEvent.onKeyUp.callback.get(key.toString()).push(callback);
            break;
        default:
            console.warn("监听类似eventName超出预期，注册监听函数失败");
    }

}

/**
 * 获取窗口尺寸
 * @return {any[]}  窗口inner尺寸
 */
export function getInnerSize() {
    return [GlobalEvent.onResize.width, GlobalEvent.onResize.height];
}
