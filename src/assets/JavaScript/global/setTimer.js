/**
 * 计时器相关代码
 */

export class timerControl {
    timer = new Map();

    /**
     * 开始计时，key为计时器的key值
     * @param key   {string}    计时器的键值
     * @return      {number}    开始时间（ms）
     */
    startTimer(key) {
        let startTime = Date.now();
        this.timer.set(key, {startTime, endTime: undefined, time: undefined});
        return startTime;
    }

    /**
     * 结束键值为key的计时器计时
     * @param key   {string}    计时器的键值
     * @return      {number}    结束计时的时间（ms）
     */
    endTimer(key) {
        if (this.timer.has(key)) {
            let timerObject = this.timer.get(key);
            timerObject.endTime = Date.now();
            timerObject.time = timerObject.endTime - timerObject.startTime;
            return timerObject.endTime;
        } else {
            console.error('未发现键值为"' + key + '"的计时器');
            console.log(this.timer);
            return 0;
        }
    }

    /**
     * 获取当前计时器中的所有记录
     * @return {[]} 计时器数据
     */
    getTimerData() {
        let data = [];
        for (let value of this.timer) {
            data.push({
                key: value[0],
                time: value[1].time,
            });
        }
        return data;
    }
}

/**
 * 设置一个默认计时器，可以直接使用
 * @type {timerControl}
 */
export let defaultTimer = new timerControl();
