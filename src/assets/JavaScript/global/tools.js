/**
 * 获取目标变量的数据类型
 * @param _var  {*} 需要识别的目标变量
 * @return {string} 目标变量的数据类型
 */
export function tools_GetType(_var) {
    switch (Object.prototype.toString.call(_var)) {
        case '[object String]':
            return 'string';
        case '[object Number]':
            return 'number';
        case '[object Boolean]':
            return 'boolean';
        case '[object Symbol]':
            return 'symbol';
        case '[object Undefined]':
            return 'undefined';
        case '[object Null]':
            return 'null';
        case '[object Function]':
            return 'function';
        case '[object Date]':
            return 'date';
        case '[object Array]':
            return 'array';
        case '[object RegExp]':
            return 'regExp';
        case '[object Error]':
            return 'error';
        case '[object HTMLDocument]':
            return 'htmlDocument';
        case '[object global]':
            return 'global';
        default:
            console.error('未知的变量类型');
            return '';
    }
}

/**
 * 深度拷贝函数,能拷贝对象和数组到一个新的变量中并返回
 * @param   {object|array}  obj     待拷贝的对象或数组
 * @return  {object|array}          拷贝得到的对象或数组的引用
 */
export function tools_DeepClone(obj) {
    const cloneObj = Array.isArray(obj) ? []:{};
    if(obj && typeof obj ==="object"){
        for(const key in obj){
            if(obj.hasOwnProperty(key)){
                if(obj[key] && typeof obj[key] === "object"){
                    cloneObj[key] = tools_DeepClone(obj[key]);
                } else{
                    cloneObj[key] = obj[key]
                }
            }
        }
    }
    return cloneObj;
}

/* *************************************** 生成不重复的数字 *************************************** */
let soleNumber = new Map();
/**
 * 根据key值返回不重复的递增数字
 * @param key   {string}    该条记录对应的键值
 * @return      {number}    递增的数值
 */
export function tools_getSoleNumber(key) {
    if (soleNumber.has(key)) {
        let num = soleNumber.get(key);
        soleNumber.set(key, ++num);
        return num;
    } else {
        soleNumber.set(key, 1);
        return 1;
    }
}
/* *************************************** 生成不重复的18位id *************************************** */
let timeId = new Map();
/**
 * 使用生成器（ES6新特性）生成不重复的id(18位长度)
 * @return {Generator<string, void, *>}
 */
function* Generator_TimeId() {
    while (true) {
        let timeId = '';
        const myDate = new Date();
        // 获取完整的年份(4位,1970-????)
        timeId += myDate.getFullYear();
        // 获取当前月份(0-11,0代表1月)
        timeId += (myDate.getMonth() >= 9 ? '' : '0') + (myDate.getMonth() + 1);
        //获取当前日(1-31)
        timeId += (myDate.getDate() >= 10 ? '' : '0') + myDate.getDate();
        // 获取当前小时数(0-23)
        timeId += (myDate.getHours() >= 10 ? '' : '0') + myDate.getHours();
        // 获取当前分钟数(0-59)
        timeId += (myDate.getMinutes() >= 10 ? '' : '0') + myDate.getMinutes();
        // 获取当前秒数(0-59)
        timeId += (myDate.getSeconds() >= 10 ? '' : '0') + myDate.getSeconds();
        for (let n1 = 0; n1 < 10; n1++)
            for (let n2 = 0; n2 < 10; n2++)
                for (let n3 = 0; n3 < 10; n3++)
                    for (let n4 = 0; n4 < 10; n4++) {
                        yield timeId + n1 + n2 + n3 + n4;
                    }
    }
}
/**
 * 根据key值返回一个不重复的18位字符串id
 * @param key   {string}    键值
 * @return      {string}    不重复的18位字符串
 */
export function tools_getTimeId18(key) {
    if (timeId.has(key)) {
        return timeId.get(key).next().value;
    } else {
        let generator = Generator_TimeId();
        timeId.set(key, generator);
        return generator.next().value;
    }
}

/* *************************************** 管理延时计时器 *************************************** */
let timeOut = new Map();
/**
 * 管理计时器 setTimeOut，可以通过同一个key值不断刷新计时器，以达到延迟执行的目的
 * @param key       {string}    为当前计时器定义一个唯一的key值
 * @param time      {number}    间隔时间，单位毫秒
 * @param callback  {function}  循环计时器回调函数, callback为空的时候将清空计时器（如果存在的话）
 */
export function tools_TimeOut(key, time, callback) {
    if (timeOut.has(key)) // 重置计时器
        clearTimeout(timeOut.get(key));
    callback && timeOut.set(key, setTimeout(callback, time));
}

/* *************************************** 管理循环计时器 *************************************** */
let interval = new Map();
/**
 * 管理循环计时器，避免重复创建
 * @param key       {string}    为当前循环计时器定义一个唯一的key值
 * @param isCreate  {boolean}   是否创建循环计时器，为false时尝试检索并销毁当前key对应的循环计时器
 * @param time      {number|undefined}      间隔时间，单位毫秒
 * @param callback  {function|undefined}    循环计时器回调函数
 */
export function tools_Interval(key, isCreate, time = 0, callback = undefined) {
    if (isCreate && callback) { // 创建循环计时器
        if (!interval.has(key)) interval.set(key, setInterval(callback, time));
    } else {        // 销毁循环计时器
        if (interval.has(key)) {
            clearInterval(interval.get(key));
            interval.delete(key);
        }
    }
}
