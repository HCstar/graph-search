/**
 * 数据结构 队列
 */
export class Queue {
    #array = undefined; // 队列载体
    #startIndex = 0;    // 队列开始的下角标，初始值为0，表示队列为空
    #endIndex = -1;     // 队列结束的下角标
    #length = 0;        // 队列的最大长度
    #currentSize = 0;   // 当前队列长度
    constructor(length) {
        this.#array = new Array(length);
        this.#length = length;
    }

    /**
     * 入队列操作
     * @param value
     */
    push(value) {   // 入队列操作
        if (this.#currentSize === this.#length) {   // 队列已满，需要进行扩容操作
            let newArray = new Array(this.#length * 2);// 数组扩容两倍
            let size = this.#currentSize;
            for (let i = 0; i < size; i++) {newArray[i] = this.pop();}
            this.#array = newArray;
            this.#currentSize = size;
            this.#startIndex = 0;
            this.#endIndex = size - 1;
            this.#length *= 2;
        }
        this.#endIndex = (this.#endIndex + 1) % this.#length;
        this.#array[this.#endIndex] = value;
        this.#currentSize++;
    }
    /**
     * 出队列操作
     */
    pop() {         // 出队列操作
        if (this.#currentSize === 0) {
            console.error("队列为空，出队列操作失败");
            return undefined;
        }
        let value = this.#array[this.#startIndex];
        this.#startIndex = (this.#startIndex + 1) % this.#length;
        this.#currentSize--;
        return value;
    }
    /**
     * 判断队列是否为空
     */
    isEmpty() {return this.#currentSize === 0;}
    /**
     * 判断队列是否已满
     */
    isFull() {return this.#currentSize === this.#length;}
    /**
     * 获取当前队列的长度
     */
    size() {return this.#currentSize;}
}
