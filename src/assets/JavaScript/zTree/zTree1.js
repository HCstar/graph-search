import * as tools from '@/assets/JavaScript/global/tools';
window.tools = tools;

/**
 * 用于捕获 zTree 上鼠标按键按下后的事件回调函数
 * @param event     {object}    标准的 js event 对象
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    被点击的节点 JSON 数据对象
 */
function onMouseDown(event, treeId, treeNode) {

}

/**
 * 用于捕获 zTree 上鼠标按键松开后的事件回调函数
 * @param event     {object}    标准的 js event 对象
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    被点击的节点 JSON 数据对象
 */
function onMouseUp(event, treeId, treeNode) {

}

/**
 * 用于捕获节点被点击的事件回调函数
 * @param event     {object}    标准的 js event 对象
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    被点击的节点 JSON 数据对象
 * @param clickFlag {number}    节点被点击后的选中操作类型
 */
function onClick(event, treeId, treeNode, clickFlag) {

}

/**
 * 图元树双击事件回调函数
 * @param event     {object}    标准的 js event 对象
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    被点击的节点 JSON 数据对象
 */
function onDblClick(event, treeId, treeNode) {
    const zTree1 = window.getThis_zTree1();
    event.stopPropagation();            // 阻止双击事件继续传播
    if (!treeNode)  return 'ignore';    // 双击位置不在节点上
    if (!treeNode.isParent) {
        const setting = window.getThis_Setting();
        setting.OpenSetting(treeNode.type, treeNode.name, treeNode.value, treeNode._icon, treeNode.color, (name, value,icon, color) => {
            treeNode.name = name;
            treeNode.value = value;
            treeNode._icon = icon;
            treeNode.color = color;
            zTree1.zTreeObject.updateNode(treeNode);
            updateIcon(treeNode);
        })
    }
}

/**
 * 用于捕获 zTree 上鼠标右键点击之后的事件回调函数
 * @param event     {object}    标准的 js event 对象
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    被点击的节点 JSON 数据对象
 */
function onRightClick(event, treeId, treeNode) {
    const zTree1 = window.getThis_zTree1()
    zTree1.zTree1_CancelSelected(undefined,undefined);
    if (treeNode && !treeNode.isParent) {
        let app = window.getThis_App();
        app.$confirm(`是否删除 "${treeNode.name}" ?`, '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
        }).then(() => {
            zTree1.zTreeObject.removeNode(treeNode);
        }).catch(() => {
            app.$message({
                type: 'info',
                message: '已取消删除'
            });
        });
    }
}

/**
 * 用于捕获节点编辑名称结束（Input 失去焦点 或 按下 Enter 键）之后，更新节点名称数据之前的事件回调函数，并且根据返回值确定是否允许更改名称的操作
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    将要更改名称的节点 JSON 数据对象
 * @param newName   {string}    修改后的新名称
 * @param isCancel  {boolean}   是否取消了操作,isCancel = true 表示取消编辑操作（按下 ESC 或 使用 cancelEditName 方法）
 */
function beforeRename(treeId, treeNode, newName, isCancel) {
    const self = window.getThis_zTree1();
    const treeObject = self.zTreeObject;
    if (isCancel) return true;
    // 是否为空名称
    if (newName.length === 0) {
        self.$message.warning("名称不能为空");
        treeObject.cancelEditName();
        return false;
    }
    // 是否为根节点
    const parentNode = treeObject.getNodeByTId(treeNode.parentTId);   // 搜索父节点
    if (!parentNode) {
        self.$message.error("不支持修改根结点的名称");
        treeObject.cancelEditName();
        return false;
    }

    // 检测是否名称冲突
    if (newName === '节点' || newName === '边') {
        self.$message.warning('名称不能与根节点相同');
        treeObject.cancelEditName();
        return false;
    }
    return true;
}

/**
 * 用于捕获节点被删除之前的事件回调函数，并且根据返回值确定是否允许删除操作
 * @param treeId
 * @param treeNode
 */
function beforeRemove(treeId, treeNode) {
    const zTree1 = window.getThis_zTree1();
    if(treeNode.parentTId !== zTree1.rootNode.tId) {
        zTree1.$message.error("不支持删除当前节点");
        return false;
    }
    zTree1.$confirm('该操作将删除"' + treeNode.name + '"结点，是否继续？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        zTree1.zTreeObject.removeNode(treeNode,false);
    }).catch(() => {
        zTree1.$message.info("删除操作已撤销");
    });
    return false;
}

/**
 * 用于当鼠标移动到节点上时，显示用户自定义控件，显示隐藏状态同 zTree 内部的编辑、删除按钮
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    需要显示自定义控件的节点 JSON 数据对象
 */
function addHoverDom(treeId, treeNode) {
    const zTree1 = window.getThis_zTree1();
    const nodeDom = $("#" + treeNode.tId + "_a");
    // 添加新增按钮
    if (!treeNode.isParent) return false;   // 如果不是父节点，则不添加;
    if (nodeDom.children('button#' + treeNode.tId + '_add').length)    return;
    const addDom = `<button id="${treeNode.tId}_add" title="新增" class='zTree1_AddBtn'></button>`;
    nodeDom.append(addDom);
    // 绑定按钮点击事件
    const addButton = nodeDom.children('button#' + treeNode.tId + '_add');
    const clickAddNode = function(event) {
        event.stopPropagation();            // 阻止双击事件继续传播
        if (event.target.id === `${zTree1.rootNode.tId}_add`)
            zTree1.zTree1_AddNode('节点', getDemo_Node('正方形', 'rect', 'red'));
        else if (event.target.id === `${zTree1.rootLine.tId}_add`)
            zTree1.zTree1_AddNode('边', getDemo_Line('无向边', 'line-1', 'red'));
    }
    if (addButton) {
        addButton.bind("click", clickAddNode);     // 绑定单击事件
        addButton.bind("dblclick", clickAddNode);  // 绑定双击事件
    }
}

/**
 * 用于当鼠标移出节点时，隐藏用户自定义控件，显示隐藏状态同 zTree 内部的编辑、删除按钮
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    需要显示自定义控件的节点 JSON 数据对象
 */
function removeHoverDom(treeId, treeNode) {
    $('button#' + treeNode.tId + '_add').unbind().remove();
}

/**
 * 添加自定义节点
 * @param treeId    {string}    对应 zTree 的 treeId，便于用户操控
 * @param treeNode  {object}    需要显示自定义控件的节点 JSON 数据对象
 */
function addDiyDom(treeId, treeNode) {
    const nodeDom = $("#" + treeNode.tId + "_a");
    if (nodeDom.children('svg#' + treeNode.tId + '_svg').length)    return;
    let iconNode = '';
    if (treeNode._icon.substr(0,5) === '#icon') { // 添加svg图像
        iconNode = `
        <svg id="${treeNode.tId}_svg" aria-hidden="true">
          <use xlink:href="${treeNode._icon}"></use>
        </svg>`;
    } else if (treeNode._icon === 'rect') {    // 添加单色矩形
        iconNode = `
        <svg id="${treeNode.tId}_svg" aria-hidden="true">
            <rect width="100%" height="100%" fill="${treeNode.color}"></rect>
        </svg>`;
    } else if (treeNode._icon === 'circle') {   // 添加单色圆
        iconNode = `
        <svg id="${treeNode.tId}_svg" aria-hidden="true" viewBox="0 0 100 100">
            <circle cx="50%" cy="50%" r="50%" fill="${treeNode.color}"></circle>
        </svg>`;
    } else if (treeNode._icon === 'line-1') {   // 无向边
        iconNode = `
        <svg id="${treeNode.tId}_svg" aria-hidden="true" viewBox="0 0 100 100">
            <line x1='0' y1='100' x2='100' y2='0' stroke="${treeNode.color}" stroke-width="10"/>
        </svg>`;
    } else if (treeNode._icon === 'line-2') {   // 有向边
        iconNode = `
        <svg id="${treeNode.tId}_svg" aria-hidden="true" viewBox="0 0 100 100">
            <line x1='0' y1='100' x2='100' y2='0' stroke="${treeNode.color}" stroke-width="10"/>
            <polygon points='60,0 100,0 100,40' fill='${treeNode.color}' stroke='${treeNode.color}'/>
        </svg>`;
    }
    nodeDom.prepend(iconNode);
}

/**
 * 更新节点的图像
 * @param treeNode
 */
export function updateIcon(treeNode) {
    // 先卸载原本的图像
    $('svg#' + treeNode.tId + '_svg').remove();
    $('span#' + treeNode.tId + '_icon').remove();
    // 添加新图像
    addDiyDom('', treeNode);
}

export function getSetting() {  // 获取zTree的setting配置参数
    return {
        callback:{ // 回调函数相关(注意：初始化时再进行绑定)
            onClick: onClick,           // 用于捕获节点被点击的事件回调函数
            onDblClick: onDblClick,     // 用于捕获节点被双击的事件回调函数
            onMouseDown: onMouseDown,   // 用于捕获 zTree 上鼠标按键按下后的事件回调函数
            onMouseUp: onMouseUp,       // 用于捕获 zTree 上鼠标按键松开后的事件回调函数
            beforeRemove: beforeRemove, // 用于捕获节点被删除之前的事件回调函数，并且根据返回值确定是否允许删除操作
            beforeRename: beforeRename, // 用于捕获节点编辑名称结束（Input 失去焦点 或 按下 Enter 键）之后，更新节点名称数据之前的事件回调函数，并且根据返回值确定是否允许更改名称的操作
            onRightClick: onRightClick, // 用于捕获 zTree 上鼠标右键点击之后的事件回调函数
        },
        data: {
            keep: {
                leaf: false,          // zTree 的节点叶子节点属性锁，是否始终保持 isParent = false
                parent: true,         // zTree 的节点父节点属性锁，是否始终保持 isParent = true
            },
        },
        edit: {
            enable: true,           // 设置 zTree 是否处于编辑状态
            showRemoveBtn: true,    // 设置是否显示删除按钮
            removeTitle: "删除",     // 删除按钮的 Title 辅助信息。
            showRenameBtn: true,    // 设置是否显示编辑名称按钮。
            renameTitle: "重命名",   // 编辑名称按钮的 Title 辅助信息。
            drag:{ // 拖拽相关
                isCopy: false,      // 拖拽时, 设置是否允许复制节点[setting.edit.enable = true 时生效]
                isMove: false       // 拖拽时, 设置是否允许移动节点[setting.edit.enable = true 时生效]
            }
        },
        view: { // (注意：初始化时再进行绑定)
            addDiyDom: addDiyDom,
            addHoverDom: addHoverDom,       // 用于当鼠标移动到节点上时，显示用户自定义控件，显示隐藏状态同 zTree 内部的编辑、删除按钮
            removeHoverDom: removeHoverDom, // 用于当鼠标移出节点时，隐藏用户自定义控件，显示隐藏状态同 zTree 内部的编辑、删除按钮
            autoCancelSelected: true,       // 点击节点时，按下 Ctrl 或 Cmd 键是否允许取消选择操作。
            dblClickExpand: false,          // 双击节点时，是否自动展开父节点的标识
            // expandSpeed: "slow",            // zTree 节点展开、折叠时的动画速度，设置方法同 JQuery 动画效果中 speed 参数。
            // fontCss : {color:"red"},     // 个性化文字样式，只针对 zTree 在节点上显示的<A>对象。
            selectedMulti: true,            // 设置是否允许同时选中多个节点。
            showIcon: false,                // 设置 zTree 是否显示节点的图标。（设置为false，手动添加图像）
            showLine: false,                // 设置 zTree 是否显示节点之间的连线。
        },
    }
}

/**
 * 获取 root结点 参数模板
 * @param   key     {string}    要获取的根节点类型
 */
export function getDemo_RootNode(key) {
    if (key === '节点') {
        return {
            name:"节点",             // 节点名称。不能重复,作为区分节点的依据之一
            _icon:"#icon-rootNode", // 节点自定义图标的 URL 路径。
            isParent: true,         // 记录 treeNode 节点是否为父节点。
            open:true,              // 记录 treeNode 节点的 展开 / 折叠 状态。
            children:[]             // 节点的子节点数据集合。
        }
    }

    if (key === '边') {
        return {
            name:"边",               // 节点名称。不能重复,作为区分节点的依据之一
            _icon: "#icon-rootLine", // 节点自定义图标的 URL 路径。
            isParent: true,         // 记录 treeNode 节点是否为父节点。
            open:true,              // 记录 treeNode 节点的 展开 / 折叠 状态。
            children:[]             // 节点的子节点数据集合。
        }
    }
}

/**
 * 图元树，人员节点，参数模板
 * @param title     节点标题
 * @param icon      节点图标
 * @param color     节点在画布上的颜色
 * @param type      节点类型
 * @param value     权值
 */
export function getDemo_Node(title, icon, color, type = 'node', value = 0) {
    return {
        name:title,
        _icon : icon,
        color: color,
        type : type,
        value: value,
    }
}

/**
 * 图元树，人员节点，参数模板
 * @param title     边名称
 * @param type      节点类型
 * @param icon      边图标
 * @param color     节点在画布上的颜色
 * @param value     权值
 */
export function getDemo_Line(title, icon, color, type = 'line', value = 0) {
    return {
        name:title,
        _icon : icon,
        color: color,
        type : type,
        value: value,
    }
}
