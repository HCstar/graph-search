import G6 from '@antv/g6';
import * as defaultEdge from './defaultEdge.js';
import el from "element-ui/src/locale/lang/el";

G6.registerEdge('line-growth', {
    /**
     * 设置节点的状态，主要是交互状态，业务状态请在 draw 方法中实现
     * 单图形的节点仅考虑 selected、active 状态，有其他状态需求的用户自己复写这个方法
     * @param  {String} name 状态名称
     * @param  {Object} value 状态值
     * @param  {Edge} edge 节点
     */
    setState(name, value, edge) {
        if (name === 'growth') {    // 正向生长
            if (value) {
                edge.getKeyShape().attr(edge.getCurrentStatesStyle());
                const animateCfg = edge.getCurrentStatesStyle().animateCfg;
                const shape = edge.getKeyShape();
                const length = shape.getTotalLength();
                animateCfg.callback = () => {
                    animateEdgeCallback(edge.getModel().id)(edge.getTarget());
                };
                shape.animate((ratio) => {  // 正向生长
                    const startLen = ratio * length;
                    return {
                        lineDash: [startLen, length - startLen],
                    };
                }, animateCfg);
            } else {
                edge.getKeyShape().stopAnimate(true);       // 停止动画
                edge.getKeyShape().attr(edge.getModel().style);
            }
        } else if (name === 'growth2') {    // 逆向生长
            if (value) {
                edge.getKeyShape().attr(edge.getCurrentStatesStyle());
                const animateCfg = edge.getCurrentStatesStyle().animateCfg;
                const shape = edge.getKeyShape();
                const length = shape.getTotalLength();
                animateCfg.callback = () => {
                    animateEdgeCallback(edge.getModel().id)(edge.getSource());
                };
                shape.animate((ratio) => {
                    const startLen = ratio * length;
                    return {
                        lineDash: [0, length - startLen, startLen, 0],
                    };
                }, animateCfg);
            } else {
                edge.getKeyShape().stopAnimate(true);       // 停止动画
                edge.getKeyShape().attr(edge.getModel().style);
            }
        } else {
            if (value) edge.getKeyShape().attr(edge.getCurrentStatesStyle());
            else edge.getKeyShape().attr(edge.getModel().style);
        }
    },
}, 'line');

/**
 * 获取 line-growth 直线默认参数
 */
export function getEdgeCfg_LineGrowth() {
    let cfg = defaultEdge.getEdgeCfg_Line();
    cfg.type = 'line-growth';
    cfg.stateStyles.growth = {  // 正向生长
        animateCfg: {
            duration: 1000, // 一次动画的时长
            // easing: '',     // 动画函数
            delay: 0,       // 延迟一段时间后执行动画
            repeat: false,  // 是否重复执行动画
            callback: undefined,        // 动画执行完时的回调函数
            pauseCallback: undefined,   // 动画暂停时（shape.pause()）的回调函数
            resumeCallback: undefined,  // 动画恢复时（shape.resume()）的回调函数
        }
    }
    cfg.stateStyles.growth2 = {  // 逆向生长
        animateCfg: {
            duration: 1000, // 一次动画的时长
            // easing: '',     // 动画函数
            delay: 0,       // 延迟一段时间后执行动画
            repeat: false,  // 是否重复执行动画
            callback: undefined,        // 动画执行完时的回调函数
            pauseCallback: undefined,   // 动画暂停时（shape.pause()）的回调函数
            resumeCallback: undefined,  // 动画恢复时（shape.resume()）的回调函数
        }
    }
    return cfg;
}



const animateCallback = new Map();
/**
 * 设置/执行节点动画执行结束后的回调函数
 * @param edgeId    {string}    节点id
 * @param callback  {function|undefined}    动画执行完之后的回调函数，该回调会接受一个参数，即生长方向的另一个节点对象
 */
export function animateEdgeCallback(edgeId, callback = undefined) {
    let fn = () => {};
    if (callback) { // 设置回调函数
        animateCallback.set(edgeId, callback);
    } else if (animateCallback.has(edgeId)) {
        fn = animateCallback.get(edgeId);
        animateCallback.delete(edgeId);
    }
    return fn;
}
