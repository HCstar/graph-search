export * from './defaultEdge';
export * from './line-growth.js';

import * as lineGrowth from './line-growth.js';

export function setEdgeActive(edge, startNode, callback) {
    const edgeModel = edge.getModel();
    edge.clearStates(edge.getStates());
    lineGrowth.animateEdgeCallback(edge.getModel().id, callback);   // 设置回调函数
    if (edgeModel.source === startNode.getModel().id) {
        edge.setState('growth', true);  // 正向生长
    } else {
        edge.setState('growth2', true);  // 正向生长
    }
}

