/**
 * 获取 所有内置的边支持的通用属性
 * @return {Object}     通用属性对象
 */
function getDefaultCfg() {
    return {
        id: '',     // 边唯一 ID，必须是唯一的 string
        source: '', // 开始点 id
        target: '', // 结束点 id
        weight: 0,  // (自定义属性)边的权值

        type: 'line',// 指定边的类型，可以是内置边的类型名称，也可以是自定义边的名称。默认为 'line'
        // sourceAnchor: '',    // 边的起始节点上的锚点的索引值
        // targetAnchor: '',    // 边的终止节点上的锚点的索引值
        style: {    // 边的样式属性
            stroke: 'black',    // 边的颜色
            lineWidth: 3,       // 边宽度
            lineAppendWidth: 20,// 边响应鼠标事件时的检测宽度，当 lineWidth 太小而不易选中时，可以通过该参数提升击中范围
            startArrow: false,  // {object|boolean} 为 true 时在边的开始端绘制默认箭头，为 false 时不绘制结束端箭头；也可以使用内置箭头配置
            endArrow: {         // {object|boolean} 为 true 时在边的结束端绘制默认箭头，为 false 时不绘制结束端箭头；也可以使用内置箭头配置
                path: 'M -6,3 L 0,0 L -6,-3',
                d: 35,
            },
            strokeOpacity: 1,   // 边透明度
            // shadowColor: '',    // 阴影颜色
            // shadowBlur: '',     // 阴影模糊程度
            // shadowOffsetX: '',  // 阴影 x 方向偏移量
            // shadowOffsetY: '',  // 阴影 y 方向偏移量
            lineDash: [1, 0],   // 设置线的虚线样式，可以指定一个数组。一组描述交替绘制线段和间距（坐标空间单位）长度的数字。 如果数组元素的数量是奇数， 数组的元素会被复制并重复。
            cursor: 'pointer',  // 鼠标在该边上时的鼠标样式，CSS 的 cursor 选项都支持
        },
        label: '',              // 文本文字，如果没有则不会显示
        labelCfg: {             // 文本配置项
            position: 'center', // 文本相对于节点的位置，目前支持的位置有：'center'，'top'，'left'，'right'，'bottom'。默认为 'center'。modelRect 节点不支持该属性
            offset: 0,          // 文本的偏移，position 为 'bottom' 时，文本的上方偏移量；position 为 'left' 时，文本的右方偏移量；以此类推在其他 position 时的情况。modelRect 节点的 offset 为左边距
            style: {            // 标签的样式属性。
                fill: 'black',      // 填充色
                stroke:'black',     // 描边颜色
                lineWidth: 1,       // 描边宽度
                opacity: 1,         // 文本透明度
                fontFamily: '',     // 文本字体
                fontSize: '15',     // 文本字体大小
            },
        },
        stateStyles: {          // 各状态下的样式
            selected: {
                lineDash: [1, 0],// 设置为实线
                lineWidth: 6,   // 边宽度
            },
            dormant: {          // 不活跃状态
                lineDash: [5, 5],   // 设置为虚线
                lineWidth: 3,       // 描边宽度
            },
        },
    }
}

/**
 * 获取直线默认参数
 */
export function getEdgeCfg_Line() {
    return getDefaultCfg();
}

/**
 * 获取折线默认参数
 */
export function getEdgeCfg_Polyline() {
    let cfg = getDefaultCfg();
    cfg.type = 'polyline';
    Object.assign(cfg.style, {
        radius: 4,  // 拐弯处的圆角弧度
        offset: 5,  // 拐弯处距离节点最小距离
    });
    return cfg;
}

/**
 * 圆弧线
 */
export function getEdgeCfg_Arc() {
    let cfg = getDefaultCfg();
    cfg.type = 'arc';
    cfg.curveOffset = 20;   // 圆弧顶端距离两线中心位置的距离, 数值绝对值大小控制圆弧的大小，正负控制圆弧弯曲的方向
    return cfg;
}

/**
 * 二阶贝塞尔曲线
 */
export function getEdgeCfg_Quadratic() {
    let cfg = getDefaultCfg();
    cfg.type = 'quadratic';
    cfg.curvePosition = 0.5;// 控制点在两端点连线上的相对位置，范围 0 ～ 1
    return cfg;
}

/**
 * 三阶贝塞尔曲线
 */
export function getEdgeCfg_Cubic() {
    let cfg = getDefaultCfg();
    cfg.type = 'cubic';
    return cfg;
}

