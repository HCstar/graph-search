import G6 from "@antv/g6";

/**
 * 获取 toolbar 插件对象
 */
export function getPlugin_Toolbar() {
    return new G6.ToolBar({
        container: undefined,       // ToolBar 容器，如果不设置，则默认使用 canvas 的 DOM 容器
        className: 'graphToolBar g6-component-toolbar',  // ToolBar 内容元素的 class 类名
        position: undefined,        // ToolBar 的位置坐标
        getContent: undefined,      // 自定义工具栏（函数类型，返回DOM元素或字符串）
        handleClick: undefined,     // 点击 ToolBar 中每个图标的回调函数
    });
}
