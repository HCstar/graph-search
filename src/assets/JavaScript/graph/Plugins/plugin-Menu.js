import G6 from '@antv/g6';
import * as AM from '@/assets/JavaScript/算法演示/AM-index';
import it from "element-ui/src/locale/lang/it";


/**
 * 获取 menu 插件对象
 * @param   type    {string}    menu菜单类型（不同模式下可能需要不同的右键菜单，可进行动态绑定）
 */
export function getPlugin_Menu(type) {
    if (type === 'menu-node-AM') {    // 节点的算法演示模型下的右键菜单
        return new G6.Menu({
            offsetX: 6,
            offsetY: 10,
            itemTypes: ['node'],
            getContent(e) {
                const outDiv = document.createElement('div');
                outDiv.style.width = '100px';
                outDiv.innerHTML =
                    `<div class="canvas-edge-menu">
    <div>设为起点</div>
    <div>设为终点</div>
</div>`
                return outDiv
            },
            handleMenuClick(target, item) {
                if (target.innerHTML === '设为起点') {
                    AM.keyNode.startNode && AM.keyNode.startNode.clearStates(['startNode', 'endNode']);
                    AM.keyNode.startNode = item;
                    item.setState('startNode', true);   // 设置新起点为选中状态
                } else if (target.innerHTML === '设为终点') {
                    AM.keyNode.endNode && AM.keyNode.endNode.clearStates(['startNode', 'endNode']);
                    AM.keyNode.endNode = item;
                    item.setState('endNode', true);   // 设置新起点为选中状态
                }
            },
        });
    }
    if (type === 'menu-edge-edit') {  // 边的右键菜单
        return new G6.Menu({
            offsetX: 6,
            offsetY: 10,
            itemTypes: ['edge'],
            getContent(e) {
                const outDiv = document.createElement('div');
                outDiv.style.width = '100px';
                outDiv.innerHTML = `
<div class="canvas-edge-menu">
    <div>反向</div>
    <div>删除</div>
</div>`
                return outDiv
            },
            handleMenuClick(target, item) {
                if (target.innerHTML === '反向') {
                    let source = item.getSource();
                    let target = item.getTarget();
                    item.setSource(target);
                    item.setTarget(source);
                    item.refresh();
                } else if (target.innerHTML === '删除') {
                    window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.removeItem(item, true);
                }
            },
        });
    }
}
