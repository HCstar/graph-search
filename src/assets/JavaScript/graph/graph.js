import * as behavior from './Behavior/Behavior-Init';
import * as plugin from "@/assets/JavaScript/graph/Plugins/plugin-Index.js";
import * as Node from './Node/Node-Init.js';
import * as Edge from './Edge/Edge-Init.js';
import * as layout from './layout/layout.js';
import G6 from '@antv/g6'

export const myGraph = {
    setGraphMode: behavior.setMode, // 设置画布模式
    getGraphCfg,    // 获取graph初始化参数
    plugin,         // graph插件相关
    Node,           // node相关
    Edge,           // edge相关
    layout,         // 布局相关
    G6,             // G6
}
/**
 * 生成一份 graph 参数模板
 */
function getGraphCfg() {
    // graph实例化参数模板
    return { // graph初始化时的默认参数
        container: undefined,       // 图的DOM容器，可以传入该DOM的id或者直接传入容器的HTML节点对象。
        width: 1900,                // 指定画布宽度，单位为 'px'。
        height: 1000,               // 指定画布高度，单位为 'px'。
        renderer: 'svg',            // 渲染方式, 'canvas' / 'svg'
        fitView: false,             // 是否开启画布自适应。开启后图自动适配画布大小。
        // fitCenter: undefined,
        /**
         * 图适应画布时，指定四周的留白。
         * 可以是一个值, 例如：fitViewPadding: 20
         * 也可以是一个数组，例如：fitViewPadding: [20, 40, 50,20]
         * 当指定一个值时，四边的边距都相等，当指定数组时，数组内数值依次对应 上，右，下，左四边的边距。
         */
        // fitViewPadding: 20,         // fitView 为 true 时生效。图适应画布时，指定四周的留白。
        groupByTypes: true,         // 各种元素是否在一个分组内，决定节点和边的层级问题，默认情况下所有的节点在一个分组中，所有的边在一个分组中，当这个参数为 false 时，节点和边的层级根据生成的顺序确定。
        // directed: undefined,
        // groupStyle: undefined,      // // groupStyle 用于指定分组的样式，详情参看 节点分组 Group 教程
        autoPaint: true,            // 当图中元素更新，或视口变换时，是否自动重绘。建议在批量操作节点时关闭，以提高性能，完成批量操作后再打开，参见后面的 setAutoPaint() 方法。
        modes: behavior.getDefaultMode(),
        defaultNode: {              // 默认状态下节点的配置，会传入draw中作为绘图的基本参数,会被写入的 data 覆盖。

        },
        defaultEdge: {              // 默认状态下边的配置，比如 type, size, color。会被写入的 data 覆盖。

        },
        // defaultCombo: undefined,
        nodeStateStyles: {          //设置不同状态下的节点样式，以便交互使用

        },
        edgeStateStyles: {          //设置不同状态下的节点样式，以便交互使用
            selected: {
                //stroke:'#ed0d10',
                lineWidth: 5,
            },
        },
        // comboStateStyles: {},
        plugins: [plugin.getPlugin_Toolbar()],        // 向 graph 注册默认插件。
        // animate: undefined,
        // animateCfg: undefined,
        minZoom: 0.2,           // 最小缩放比例
        maxZoom: 10,            // 最大缩放比例
        groupType: 'circle',    // 节点分组类型，支持 circle 和 rect
        linkCenter: true,       // Edge 是否连接到节点中间
        enabledStack: true,     // 设置为true，启用 redo & undo 栈功能
        maxStep: 100,           // redo & undo 最大步数, 只有当 enabledStack 为 true 时才起作用
        layout: {
            //布局算法的参数
        },
    };
}
