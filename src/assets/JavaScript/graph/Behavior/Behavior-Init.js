export { behaviorCfg } from './内置Behavior.js';

import './click-add-node-edge.js';
import './keyUp-Edit.js';
import './dblClick_openSetting.js';
import './globalBehavior.js'

import {behaviorCfg} from "@/assets/JavaScript/graph/Behavior/内置Behavior";
import * as plugin from '@/assets/JavaScript/graph/Plugins/plugin-Index.js'

export function getDefaultMode() {
    return {                    // 设置画布的模式。
        default: [
            behaviorCfg.get('drag-node'),   // 拖拽节点
            behaviorCfg.get('click-select'),// 点选
            behaviorCfg.get('brush-select'),// 框选
            'click:add-node-edge',          // 点击画布添加节点与边
            'keyUp:Edit',
            'dblClick:openSetting',         // 双击设置
            'globalBehavior',
        ],
        viewMode: [     // 观看模式
            behaviorCfg.get('drag-canvas'), // 拖拽画布 FIXME 按住alt/ctrl/shift等功能键都会使拖拽画布失效
            behaviorCfg.get('zoom-canvas'), // 画布缩放
            'globalBehavior',
        ],
    }
}

let plugins = {
    menu: {
        edgeEdit: undefined,
        nodeAM: plugin.getPlugin_Menu('menu-node-AM'),
    }
}
export function setMode(graph, type) {
    graph.setMode(type);
    if (type === 'default') {   // 模式被设置为default
        plugins.menu.nodeAM && graph.removePlugin(plugins.menu.nodeAM);  // 移除节点的右键菜单
        plugins.menu.nodeAM = undefined;
        plugins.menu.edgeEdit || (plugins.menu.edgeEdit = plugin.getPlugin_Menu('menu-edge-edit')); // 重新生成一个插件
        graph.addPlugin(plugins.menu.edgeEdit);   // 注册边的右键菜单

        const nodes = graph.getNodes();
        const edges = graph.getEdges();
        nodes.forEach(node => {
            node.clearStates(node.getStates()); // 清除原本的所有状态
        });
        edges.forEach(edge => {
            edge.clearStates(edge.getStates()); // 清除原本的所有状态
        });
    } else if (type === 'viewMode') {
        plugins.menu.edgeEdit && graph.removePlugin(plugins.menu.edgeEdit);  // 移除边的右键菜单
        plugins.menu.edgeEdit = undefined;
        plugins.menu.nodeAM || (plugins.menu.nodeAM = plugin.getPlugin_Menu('menu-node-AM')); // 重新生成一个插件
        graph.addPlugin(plugins.menu.nodeAM);   // 注册节点的右键菜单

        const nodes = graph.getNodes();
        const edges = graph.getEdges();
        nodes.forEach(node => {
            node.clearStates(node.getStates()); // 清除原本的所有状态
            node.setState('dormant', true);
        });
        edges.forEach(edge => {
            edge.clearStates(edge.getStates());// 清除原本的所有状态
            edge.setState('dormant', true);    // 设置状态为不活跃状态
        });
    }
}
