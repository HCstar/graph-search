export const behaviorCfg = new Map();
behaviorCfg.set('drag-canvas', {
    type: 'drag-canvas',// 拖拽画布
    direction: 'both',  // 允许拖拽方向，支持'x'，'y'，'both'，默认方向为 'both'。
    enableOptimize: false, // 是否开启优化，开启后拖动画布过程中隐藏所有的边及节点上非 keyShape 部分
    scalableRange: 0,   // 拖动 canvas 可扩展的范围
});

behaviorCfg.set('zoom-canvas', {
    type: 'zoom-canvas',// 缩放画布；
    sensitivity: 5,     // 缩放灵敏度，支持 1-10 的数值，默认灵敏度为 5。
    minZoom: 0.2,       // 最小缩放比例；
    maxZoom: 10,        // 最大缩放比例；
    enableOptimize: false, // 是否开启性能优化，设置为 true 开启，开启后缩放比例小于 optimizeZoom 时自动隐藏非 keyShape；
    // optimizeZoom: 0.2,  // 当 enableOptimize 为 true 时起作用，表示当缩放到哪个比例时开始隐藏非 keyShape；
});

behaviorCfg.set('drag-node', {
    type: 'drag-node',  // 拖拽节点
    delegateStyle: {    // 节点拖拽时的绘图属性
        strokeOpacity: 0.6,
        fillOpacity: 0.6
    },
    updateEdge: true,       // 是否在拖拽节点时更新所有与之相连的边，默认为 true 。
    enableDelegate: false,  // 拖动节点过程中是否启用 delegate，即在拖动过程中是否使用方框代替元素的直接移动
});

behaviorCfg.set('click-select', {
    type: 'click-select',   // 点击选中节点，再次点击节点或点击 Canvas 取消选中状态；
    multiple: true,         // 是否允许多选，默认为 true，当设置为 false，表示不允许多选，此时 trigger 参数无效。
    trigger: 'ctrl',        // 指定按住哪个键进行多选，默认为 shift，按住 Shift 键多选，用户可配置 shift、ctrl、alt；
});

behaviorCfg.set('tooltip', {
    type: 'tooltip',        // 节点文本提示；提示：由于 G6 没有内置任何 tooltip 的样式，用户需要自己定义样式
    formatText: (model) => {// 格式化函数，可以返回文本或者 HTML；
        return  '提示文本';
    }
});

behaviorCfg.set('edge-tooltip', {
    type: 'edge-tooltip',
    formatText: (model) => {// 格式化函数，可以返回文本或者 HTML；
        return  '提示文本';
    }
});

behaviorCfg.set('activate-relations', {
    type: 'activate-relations', // 当鼠标移到某节点时，突出显示该节点以及与其直接关联的节点和连线；
    trigger: 'mouseenter',  // 可以是 mousenter，表示鼠标移入时触发；也可以是 click，鼠标点击时触发；
    activeState: 'active',  // 活跃节点状态。当行为被触发，需要被突出显示的节点和边都会附带此状态，默认值为 active；可以与 graph 实例的 nodeStyle 和 edgeStyle 结合实现丰富的视觉效果。
    inactiveState: 'inactive',  // 非活跃节点状态。不需要被突出显示的节点和边都会附带此状态。默认值为 inactive。可以与 graph 实例的 nodeStyle 和 edgeStyle 结合实现丰富的视觉效果；
    resetSelected: false,   // 高亮相连节点时是否重置已经选中的节点，默认为 false，即选中的节点状态不会被 activate-relations 覆盖。
});

behaviorCfg.set('brush-select', {
    type: 'brush-select',   // 拖动框选节点；
    brushStyle: {           // 拖动框选框的样式
        fill: 'rgba(51,51,255,0.34)',
        fillOpacity: 0.3,
        stroke: 'rgba(0,16,64,0.81)',
        lineWidth: 1
    },
    selectedState: 'selected',  // 选中的状态，默认值为 'selected'；
    includeEdges: true,     // 框选过程中是否选中边，默认为 true，用户配置为 false 时，则不选中边；
    /*
     * 'shift'：按住 Shift 键进行拖动框选；
     * 'ctrl' / 'control'：按住 Ctrl 键进行拖动框选；
     * 'alt'：按住 Alt 键进行拖动框选；
     * 'drag'：不需要按任何键，进行拖动框选，如果同时配置了 drag-canvas，则会与该选项冲突。
     */
    trigger: 'drag',        // 触发框选的动作，默认为 'shift'，即用户按住 Shift 键拖动就可以进行框选操作
});

behaviorCfg.set('collapse-expand-group', {
    type: 'collapse-expand-group',  // 收起和展开群组；
    trigger: 'dblclick'     // 收起和展开节点分组的方式。支持 'click' 和 'dblclick' 两种方式。默认为 'dblclick'，即双击。
});

behaviorCfg.set('drag-group', {
    type: 'drag-group',     // 拖动节点分组；
    delegateStyle: {    // 拖动节点分组时的绘图属性
        strokeOpacity: 0.6,
        fillOpacity: 0.6
    },
});

behaviorCfg.set('drag-node-with-group', {
    type: 'drag-node-with-group',   // 拖动节点分组中的节点；
    delegateStyle: {    // 拖动节点时的绘图属性
        strokeOpacity: 0.6,
        fillOpacity: 0.6
    },
    maxMultiple: undefined,
    minMultiple: undefined,
});
