import G6 from '@antv/g6';

G6.registerBehavior('keyUp:Edit', {
    getEvents() {
        return {
            'keyup': 'onKeyUp',
        }
    },

    /**
     * 节点的 keyup 事件
     * @param event {object}    事件对象
     */
    onKeyUp(event) {
        const  menuEdit = window.getThis_menuEdit();
        switch (event.keyCode) {
            case 46:    // delete键
                menuEdit.ctrlEdit('delete');
                break;
            case 65:    // A
                menuEdit.ctrlEdit('selectAll');
                break;
            case 67:    // C
                menuEdit.ctrlEdit('copy');
                break;
            case 86:    // V
                menuEdit.ctrlEdit('stick');
                break;
            case 88:    // X
                menuEdit.ctrlEdit('shear');
                break;
            case 90:    // Z
                menuEdit.ctrlEdit('revocation');
                break;
        }
    },
});

