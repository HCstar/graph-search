import G6 from '@antv/g6';

G6.registerBehavior('globalBehavior', {
    getEvents() {
        return {
            'nodeselectchange': 'selectChange',
        }
    },
    /**
     * 当节点/边的选中状态改变时触发
     */
    selectChange(target) {
        const menuEdit = window.getThis_menuEdit();
        const items = target.selectedItems;
        menuEdit.hasSelectedNode = (items.nodes && items.nodes.length);
        menuEdit.hasSelectedEdge = (items.edges && items.edges.length);
    },
});

