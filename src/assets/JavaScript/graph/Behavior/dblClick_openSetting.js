import G6 from '@antv/g6';

G6.registerBehavior('dblClick:openSetting', {
    getEvents() {
        return {
            'node:dblclick': 'nodeDblClick',
            'edge:dblclick': 'edgeDblClick',
        }
    },
    /**
     * 节点的 dblclick 事件
     * @param event {object}    事件对象
     */
    nodeDblClick(event) {
        const setting = window.getThis_Setting();
        let node = event.item;
        let model = node._cfg.model;
        setting.OpenSetting('node', model.label.slice(0, model.label.lastIndexOf('(')), model.weight, model.type, model.style.fill,
            (name, weight, type, color) => {
            node.update({
                label: name + `(${weight})`,
                weight, type,
                style: {
                    fill: color,
                    stroke: color,
                }
            });
        });
    },
    /**
     * 边的 dblclick 事件
     * @param event {object}    事件对象
     */
    edgeDblClick(event) {
        const setting = window.getThis_Setting();
        let edge = event.item;
        let model = edge._cfg.model;
        setting.OpenSetting('line', model.label.slice(0, model.label.lastIndexOf('(')), model.weight,
            (model.style.endArrow === false? 'line-1' : 'line-2'), model.style.stroke,
            (name, weight, type, color) => {
                edge.update({
                    label: name + `(${weight})`, weight,
                    style: {
                        endArrow: (type === 'line-1' ? false : {
                            path: 'M -6,3 L 0,0 L -6,-3',
                            d: 35,
                        }),
                        fill: color,
                        stroke: color,
                    }
                });
        });
    },
});
