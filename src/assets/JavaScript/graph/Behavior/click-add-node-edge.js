import G6 from '@antv/g6';
import * as graphNode from "@/assets/JavaScript/graph/Node/Node-Init.js";
import * as graphEdge from '@/assets/JavaScript/graph/Edge/Edge-Init.js';

import * as tools from "@/assets/JavaScript/global/tools.js";

let _addEdge = {    // 添加边时的辅助变量
    graph: undefined,
    firstNode: undefined,
}

G6.registerBehavior('click:add-node-edge', {
    getEvents() {
        return {
            'click': 'onClick',
            'node:click': 'onNodeClick',
            'canvas:click': 'onCanvasClick'
        }
    },
    /**
     * click 事件
     * @param event {object}    事件对象
     */
    onClick(event) {
        const currentGraph = window.getThis_mainTabs().tab1_GetCurrentGraphCanvas();
        // 添加图元树上的节点
        const nodes = window.getThis_zTree1().zTree1_GetSelectedNode(); // 获取图元树上被选中的节点
        if (nodes && nodes.length && nodes[0].type === 'node') {    // 判断图元树上是否有选中的节点
            let move = 0;
            nodes.forEach(value => {
                let nodeCfg = undefined;
                if (value.zTree_Type === 'circle')
                    nodeCfg = graphNode.getNodeCfg_Circle();  // 获取默认参数
                else if(value.zTree_Type === 'rect')
                    nodeCfg = graphNode.getNodeCfg_Rect();
                Object.assign(nodeCfg, {
                    id: tools.tools_getTimeId18('group-item'),
                    weight: value.zTree_value,
                    x: event.x + move,
                    y: event.y + move,
                    // label: `${value.zTree_Name}(${value.zTree_value})`,
                    label: `${value.zTree_Name}(${graphNode.getitemcount(value.zTree_Type)})`,
                });
                move += 50;
                console.log('value', value);
                nodeCfg && (nodeCfg.style.fill = value.zTree_Color) && (nodeCfg.style.stroke = value.zTree_Color);
                console.log(nodeCfg);
                currentGraph.graphObj.addItem('node', nodeCfg);
                // TODO 画布自动扩充判断
            });
        }
    },
    /**
     * 画布的 click 事件
     * @param event {object}    事件对象
     */
    onCanvasClick(event) {
        _addEdge.graph = undefined; // 清除状态
        _addEdge.firstNode = undefined; // 清除状态
    },
    /**
     * 结点的 click 事件
     * @param event {object}    事件对象
     */
    onNodeClick(event) {
        // TODO 完善添加边的逻辑, 两个节点之间只能存在一条边，添加边的时候应该判断，避免重复。
        const currentGraph = window.getThis_mainTabs().tab1_GetCurrentGraphCanvas();
        const nodes = window.getThis_zTree1().zTree1_GetSelectedNode(); // 获取图元树上被选中的节点
        if (nodes && nodes.length && nodes[0].type === 'line') {    // 判断图元树上是否有选中的边
            if (_addEdge.firstNode && _addEdge.graph.graphId === currentGraph.graphId) {
                let cfg = graphEdge.getEdgeCfg_LineGrowth();
                cfg.id = tools.tools_getTimeId18('group-item');
                // cfg.weight = nodes[0].zTree_value;
                console.log(cfg.source);
                cfg.source = _addEdge.firstNode.getID();
                cfg.target = event.item.getID();
                let graph_=currentGraph.graphObj;
                let dx=Math.abs(graph_.findById(cfg.source)._cfg.model.x-graph_.findById(cfg.target)._cfg.model.x);
                let dy=Math.abs(graph_.findById(cfg.source)._cfg.model.y-graph_.findById(cfg.target)._cfg.model.y);
                cfg.weight=parseInt(Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2)));
                cfg.style.stroke = nodes[0].zTree_Color;
                cfg.label = `${nodes[0].zTree_Name}(${cfg.weight})`;
                // cfg.label = `${nodes[0].zTree_Name}(${nodes[0].zTree_value})`;
                if (nodes[0].zTree_Type === 'line-1') {
                    cfg.style.endArrow = false;
                    currentGraph.graphObj.addItem('edge', cfg);
                } else if (nodes[0].zTree_Type === 'line-2') {
                    currentGraph.graphObj.addItem('edge', cfg);
                }
                event.item.clearStates('selected'); // 清除第二个节点的选中状态
                _addEdge.graph = undefined; // 清除状态
                _addEdge.firstNode = undefined; // 清除状态
            } else {
                _addEdge.graph = currentGraph;
                _addEdge.firstNode = event.item;
            }
        } else {
            _addEdge.graph = undefined; // 清除状态
            _addEdge.firstNode = undefined; // 清除状态
        }
    }
});
