/**
 * 获取 所有内置的节点支持的通用属性
 * @return {Object}     通用属性对象
 */
function getDefaultCfg() {
    return {
        x: 0,       // 坐标x
        y: 0,       // 坐标y
        id: '',
        weight: 0,  // (自定义属性)边的权值
        father_id:'',
        f_value:0,
        g_value:0,
        h_value:0,
        to_star:99999,
        type: "circle",     // 元素的类型，不传则使用默认值
        size: [80, 80],
        anchorPoints: undefined,    // 指定边连入节点的连接点的位置（相对于该节点而言），可以为空。例如: [0, 0]，代表节点左上角的锚点，[1, 1],代表节点右下角的锚点
        linkPoints: undefined,  // 视觉上的四个锚点, 默认不显示，应与 anchorPoints 配合使用
        label: '',          // 文本文字
        labelCfg: {         // 文本配置项
            position: 'center',// 文本相对于节点的位置，目前支持的位置有：'center'，'top'，'left'，'right'，'bottom'。默认为 'center'。modelRect 节点不支持该属性
            offset: 0,      // 文本的偏移，position 为 'bottom' 时，文本的上方偏移量；position 为 'left' 时，文本的右方偏移量；以此类推在其他 position 时的情况。modelRect 节点的 offset 为左边距
            style: {        // 标签的样式属性。
                fill: 'black',      // 填充色
                stroke:'black',     // 描边颜色
                lineWidth: 1,       // 描边宽度
                opacity: 1,         // 文本透明度
                fontFamily: '',     // 文本字体
                fontSize: '15',     // 文本字体大小
            },
        },
        style: {    // 元素样式
            fill: 'white',          // 填充色
            fillOpacity: 1,         // 透明度
            stroke:'black',         // 描边颜色
            lineWidth: 1,           // 描边宽度
            radius: [5, 5, 5, 5],   // 定义圆角，支持整数或数组形式，分别对应左上、右上、右下、左下角的半径
        },
        stateStyles: {      // 各状态下的样式, 只对 keyShape 起作用
            selected: { // 选中状态
                stroke:'black',     // 描边颜色
                lineWidth: 3,       // 描边宽度
                fillOpacity: 1,     // 透明度
            },
            startNode: {
                stroke:'yellow',    // 描边颜色
                lineWidth: 9,       // 描边宽度
                fillOpacity: 1,     // 透明度
            },
            endNode: {
                stroke:'green',     // 描边颜色
                lineWidth: 9,       // 描边宽度
                fillOpacity: 1,     // 透明度
            },
            dormant: {  // 不活跃状态
                // stroke:'green',     // 描边颜色
                lineWidth: 1,       // 描边宽度
                fillOpacity: 0.3,   // 透明度
            },
        },
    }
}

/**
 * 获取circle类型节点的默认参数
 */
export function getNodeCfg_Circle() {
    let cfg = getDefaultCfg();
    cfg.type = 'circle';
    cfg.label = 'circle';
    cfg.anchorPoints = [
        [0.5, 0],
        [0, 0.5],
        [1, 0.5],
        [0.5, 1],
    ];
    return cfg;
}

export function maketostart(){

}

export function getNodeCfg_Rect() {
    let cfg = getDefaultCfg();
    cfg.type = 'rect';
    cfg.label = 'rect';
    cfg.anchorPoints = [
        [0, 0],
        [0.5, 0],
        [1, 0],
        [0, 0.5],
        [1, 0.5],
        [0, 1],
        [0.5, 1],
        [1, 1],
    ];
    return cfg;
}

/*
获取graph中正方形或圆形图元的个数，便于在label上添加序号
* */
export function getitemcount(type){
    let graph=window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj;
    let node=graph.getNodes();
    let rectnode=[];
    let circlenode=[];
    for (let i=0;i<node.length;i++){
        if (node[i]._cfg.model.type==='rect')
            rectnode.push(node[i]);
        if (node[i]._cfg.model.type==='circle')
            circlenode.push(node[i]);
    }
    switch (type){
        case 'rect':
            return rectnode.length+1;
            break;
        case 'circle':
            return circlenode.length+1;
            break;
        default:
            return undefined;
    }
}