
export function getLayoutCfg(type) {
    let cfg = null;
    switch (type) {
        case 'random':
            cfg = {
                type: 'random',             // 布局类型(非必须)
                // center: [900, 500],         // 布局中心(非必须)
                // width: undefined,           // 图的宽度(非必须)
                // height: undefined,          // 图的高度(非必须)
                workerEnabled: true         // 是否启用web-worker以防止计算时间过长(非必须)
            }
            break;
        case 'force':
            cfg = {
                type: 'force',
                // center: [900, 500],         // 可选，默认为图的中心
                // linkDistance: undefined,    // 可选，边长
                // nodeStrength: undefined,    // 可选，节点作用力，正数代表节点之间的引力作用，负数代表节点之间的斥力作用。可以使用回调函数的形式对不同对节点定义不同节点作用力
                // edgeStrength: undefined,    // 可选，边的作用力，默认根据节点的出入度自适应。可以使用回调函数的形式对不同对节点定义不同边作用力
                preventOverlap: true,       // 可选，是否防止重叠，必须配合下面属性 nodeSize，只有设置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测
                collideStrength: 1,         // 可选，防止重叠的力强度，范围 [0, 1]
                // nodeSize: 150,              // 可选，节点大小（直径）。用于碰撞检测。若不指定，则根据传入的节点的 size 属性计算。若即不指定，即节点中也没有 size，则默认大小为 10
                nodeSpacing: 10,            // 可选，preventOverlap 为 true 时生效，防止重叠时节点边缘间距的最小值。可以是回调函数，为不同节点设置不同的最小间距
                alphaDecay: 0.05,           // 可选，迭代阈值的衰减率。范围 [0, 1]，0.028 对应迭代数为 300
                // alphaMin: undefined,        // 可选，停止迭代的阈值
                // alpha: undefined,           // 可选，当前阈值
                // forceSimulation: null,      // 可选，自定义 force 方法，若不指定，则使用 d3 的方法。
                onTick: () => {             // 可选，每一次迭代的回调函数
                    console.log('ticking');
                },
                onLayoutEnd: () => {        // 可选，布局完成后的回调函数
                    console.log('force layout done');
                },
                workerEnabled: true         // 可选，是否启用web-worker以防止计算时间过长
            };
            break;
        case 'grid':
            cfg = {
                type: 'grid',
                begin: [0, 0],              // 可选，网格开始位置（左上角）
                preventOverlap: true,       // 可选，是否防止重叠，必须配合下面属性 nodeSize，只有设置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测
                nodeSize: 150,              // 可选，节点大小（直径）。用于防止节点重叠时的碰撞检测
                preventOverlapPadding: 20,  // 可选，避免重叠时节点的间距 padding。preventOverlap 为 true 时生效
                condense: true,             // 可选，为 false 时表示利用所有可用画布空间，为 true 时表示利用最小的画布空间
                // rows: undefined,            // 可选，网格的行数，为 undefined 时算法根据节点数量、布局空间、cols（若指定）自动计算
                // cols: undefined,            // 可选，网格的列数，为 undefined 时算法根据节点数量、布局空间、rows（若指定）自动计算
                sortBy: 'degree',           // 可选，指定排序的依据（节点属性名），数值越高则该节点被放置得越中心。若为 undefined，则会计算节点的度数，度数越高，节点将被放置得越中心

                workerEnabled: true         // 可选，是否启用web-worker以防止计算时间过长
            };
            break;
        case 'circular':
            cfg = {
                type: 'circular',
                // center: [900, 500],         // 可选，默认为图的中心
                radius: (() => {         // 可选，圆的半径。若设置了 radius，则 startRadius 与 endRadius 不生效
                    return window.getThis_mainTabs().tab1_GetCurrentGraphCanvas().graphObj.getHeight() / 2;
                })(),
                // startRadius: undefined,     // 可选，螺旋状布局的起始半径
                // endRadius: undefined,       // 可选，螺旋状布局的结束半径
                clockwise: true,            // 可选，是否顺时针排列
                // divisions: undefined,       // 可选，节点在环上的分段数（几个段将均匀分布），在 endRadius - startRadius != 0 时生效
                // ordering: 'degree',         // 可选，节点在环上排序的依据。默认 null 代表直接使用数据中的顺序。'topology' 按照拓扑排序。'degree' 按照度数大小排序
                // angleRatio: undefined,      // 可选，从第一个节点到最后节点之间相隔多少个 2*PI

                workerEnabled: true         // 可选，是否启用web-worker以防止计算时间过长
            };
            break;
        case 'concentric':
            cfg = {
                type: 'concentric',
                // center: [900, 500],         // 可选，默认为图的中心
                preventOverlap: true,       // 可选，是否防止重叠，必须配合下面属性 nodeSize，只有设置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测
                // nodeSize: 150,              // 可选，节点大小（直径）。用于防止节点重叠时的碰撞检测
                minNodeSpacing: 170,        // 可选，环与环之间最小间距，用于调整半径
                // sweep: undefined,           // 可选，第一个节点与最后一个节点之间的弧度差。若为 undefined ，则将会被设置为  2 _ Math.PI _ (1 - 1 / |level.nodes|) ，其中 level.nodes 为该算法计算出的每一层的节点，|level.nodes| 代表该层节点数量
                equidistant: false,         // 可选，环与环之间的距离是否相等
                // startAngle: undefined,      // 可选，开始方式节点的弧度
                clockwise: true,            // 可选，是否按照顺时针排列
                // maxLevelDiff: undefined,    // 可选，每一层同心值的求和。若为 undefined，则将会被设置为 maxValue / 4 ，其中 maxValue 为最大的排序依据的属性值。例如，若 sortBy 为 'degree'，则 maxValue 为所有节点中度数最大的节点的度数
                sortBy: 'degree',           // 可选，指定排序的依据（节点属性名），数值越高则该节点被放置得越中心。若为 undefined，则会计算节点的度数，度数越高，节点将被放置得越中心

                workerEnabled: true         // 可选，是否启用web-worker以防止计算时间过长
            };
            break;
    }
    return cfg;
}
